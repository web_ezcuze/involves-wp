<?php /* Template Name: Página VIRADA DE LOTE */ ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Agile Experience | A Maior Experiência em Trade Marketing</title>
	
	<meta name="description" content="Description">
	<meta name="keywords" content="Keywords">
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<meta name="robots" content="noindex,follow"/>
	<link rel="canonical" href="https://agilexperience.com.br" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="object" />
	<meta property="og:title" content="2017 | Agile Experience" />
	<meta property="og:url" content="https://agilexperience.com.br" />
	<meta property="og:site_name" content="Agile Experience" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="2017 | Agile Experience" />

	<?php wp_head(); ?>
</head>
<body>
	<section id="home" class="parallax">
		<header class="header header--inner">
			<a href="#home" class="logo-white scroll">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
				<style type="text/css">
					.sta0{fill:#FFF !important;}
					.sta1{fill:#FFF !important;}
				</style>
				<g id="XMLID_12_">
					<path id="XMLID_17_" class="sta0" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
						L26.3,10.8z"/>
					<path id="XMLID_15_" class="sta0" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
					<path id="XMLID_13_" class="sta1" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
						L122,29.6H114.5z"/>
				</g>
				</svg>
			</a>
			<a href="#home" class="logo-red scroll">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
				<style type="text/css">
					.sta2{fill:#FFF !important;}
					.sta3{fill:#BC2026 !important;}
				</style>
				<g id="XMLID_12_">
					<path id="XMLID_17_" class="sta2" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
						L26.3,10.8z"/>
					<path id="XMLID_15_" class="sta2" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
					<path id="XMLID_13_" class="sta3" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
						L122,29.6H114.5z"/>
				</g>
				</svg>
			</a>
			<nav class="nav">
				<a href="#home" class="nav__link scroll">Home</a>
				<a href="#aex-2017" class="nav__link scroll">AEx 2017</a>
				<a href="#programacao" class="nav__link scroll">Programação</a>
				<a href="#o-local" class="nav__link scroll">O Local</a>
				<a href="#hospedagem" class="nav__link scroll">Hospedagem</a>
				<a href="#contato" class="nav__link scroll">Contato</a>
				<a href="#pre-venda" class="nav__link scroll btn btn--menu">INSCREVA-SE AGORA</a>
				<div id="hamburger" class="hamburger">
			      <div class="hamburger__all">
			        <hr class="line_1">
			        <hr class="line_2">
			        <hr class="line_3">
			      </div>
			    </div>
			</nav>
		</header>
		<div class="slick--parallax">
			<div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fundo-escuro2-1.jpg" class="background desktop">
				<figure class="isall">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
					<style type="text/css">
						.sta4{fill: #5f0f0e !important;}
					</style>
					<g id="XMLID_12_">
						<path id="XMLID_17_" class="sta4" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
							L26.3,10.8z"/>
						<path id="XMLID_15_" class="sta4" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
						<path id="XMLID_13_" class="sta4" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
							L122,29.6H114.5z"/>
					</g>
					</svg>
				</figure>
				<div class="conteudo">
					A maior experiência em trade marketing
				</div>
			</div>
			<div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fundo-escuro3.jpg" class="background desktop">
				<figure class="isall">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
					<style type="text/css">
						.sta5{fill: #2f804d !important;}
					</style>
					<g id="XMLID_12_">
						<path id="XMLID_17_" class="sta5" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
							L26.3,10.8z"/>
						<path id="XMLID_15_" class="sta5" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
						<path id="XMLID_13_" class="sta5" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
							L122,29.6H114.5z"/>
					</g>
					</svg>
				</figure>
				<div class="conteudo">
					21 e 22 de setembro no Costão do Santinho Resort, em Florianópolis
				</div>
			</div>
			<div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fundo-escuro4.jpg" class="background desktop">
				<figure class="isall">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
					<style type="text/css">
						.sta6{fill: #1d3973 !important;}
					</style>
					<g id="XMLID_12_">
						<path id="XMLID_17_" class="sta6" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
							L26.3,10.8z"/>
						<path id="XMLID_15_" class="sta6" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
						<path id="XMLID_13_" class="sta6" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
							L122,29.6H114.5z"/>
					</g>
					</svg>
				</figure>
				<div class="conteudo">
					Dois dias de conteúdo com mais de 30 palestrantes
				</div>
			</div>
		</div>
		<div data-aos="fade-left" data-aos-duration="600" data-aos-delay="2500" class="z3 tag">
			<h1>INSCRIÇÕES</h1><br>
			<span><span style="text-decoration: line-through;display: inline-block;font-size: .6em;">PRÉ-VENDA R$ 350,00</span> <span style="background-color: #fbbf42;color: #000;padding: 6px;border-radius: 4px;font-weight: 700;display: inline-block;font-size: 1em;"><span style="transform: rotate(-6deg);display: inline-block;font-size: .6em;">Sold Out</span></span></span>
			<span style="font-size: 2em; margin-top: 5%;"><i class="italic">1º LOTE  R$ 600,00</i></span>
			<a href="#pre-venda" class="btn scroll">Inscreva-se</a>
		</div>
	</section>
	<section id="aex-2017" class="aex-2017 page page--um no-overflow z2">
		<div class="container">
			<div class="center flex">
				<div class="content mobile-one-whole col-1-2 fl box" data-aos="fade-up" data-aos-duration="600">
					<h1 class="logo">
						<figure  class="col-2-5">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
							<style type="text/css">
								.sta7{fill:#000 !important;}
								.sta8{fill:#BC2026 !important;}
							</style>
							<g id="XMLID_12_">
								<path id="XMLID_17_" class="sta7" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
									L26.3,10.8z"/>
								<path id="XMLID_15_" class="sta7" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
								<path id="XMLID_13_" class="sta8" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
									L122,29.6H114.5z"/>
							</g>
							</svg>
						</figure>
					</h1>
					<h2 class="sub-title">Único.</h2>
					<h2 class="sub-title">Completo.</h2>
					<h2 class="sub-title bottom">Inovador.</h2>
					<p class="paragraph">A maior experiência em trade marketing do Brasil: a proposta do <b class="bold">Agile Experience</b> é reunir profissionais de trade marketing para discutir desafios e práticas do segmento. O evento oferece palestras, networking e tradução simultânea.</p>
					<p class="paragraph">A terceira edição, em 2017, acontecerá nos dias 21 e 22 de setembro, no Costão do Santinho Resort, em Florianópolis. Uma oportunidade imperdível e já consolidada como referência no universo do trade marketing.</p>
				</div>
				<div class="imgs mobile-one-whole col-2-5 fl" data-aos="fade-up" data-aos-duration="600" data-aos-delay="500">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img1.jpg" alt="" class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img2.jpg" alt="" class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img3.jpg" alt="" class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img4.jpg" alt="" class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img5.jpg" alt="" class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img6.jpg" alt="" class="img">
				</div>
			</div>
		</div>
	</section>
	<section class="info">
		<article class="info__item">
			<div class="col-1-2 mobile-four-fifths flex center" style="justify-content: space-between;">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon1.svg" class="fl info__img" alt="Conteúdo" data-aos="fade-up" data-aos-duration="600">
				<div class="fl col-2-5 mobile-one-whole" style="align-self: center;">
					<h1>Conteúdo</h1>
					<p>Nomes importantes do trade marketing apresentam tendências, cases e desafios do mercado.</p>
				</div>
				<div class="clear"></div>
			</div>
		</article>
		<article class="info__item">
			<div class="col-1-2 mobile-four-fifths flex center" style="justify-content: space-between;">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon2.svg" class="fr info__img info__img--mobile" alt="Networking" data-aos="fade-up" data-aos-duration="600">
				<div class="fl col-2-5 mobile-one-whole" style="align-self: center;">
					<h1>Networking</h1>
					<p>1.000 profissionais de trade marketing reunidos em um só lugar para troca de experiências e negócios.</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon2.svg" class="fr info__img info__img--desktop" alt="Networking" data-aos="fade-up" data-aos-duration="600">
				<div class="clear"></div>
			</div>
		</article>
		<article class="info__item">
			<div class="col-1-2 mobile-four-fifths flex center" style="justify-content: space-between;">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon3.svg" class="fl info__img" alt="Feira de Negócios" data-aos="fade-up" data-aos-duration="600">
				<div class="fl col-2-5 mobile-one-whole" style="align-self: center;">
					<h1>Feira de Negócios</h1>
					<p>Marcas e profissionais reunidos em um mesmo ambiente para gerar boas ideias e ótimos negócios.</p>
				</div>
				<div class="clear"></div>
			</div>
		</article>
	</section>
	<section class="bar">
		Supreenda-se com o AEX 2016 e <a href="#pre-venda" class="scroll">garanta sua vaga</a> nessa edição. 
	</section>
	<section class="video">
		<div class="container">
			<div class="video__container">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/cH8I75zZrGA" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
	<section id="programacao">
		<div class="container">
			<h1 style="margin: 4% 0;" class="title">Programação</h1>
		</div>
		<div class="container">
			<p style="margin: 4% 0;font-size: 2em;" class="col-4-5 mobile-one-whole">As palestras e atividades que irão compor a programação do AEx estão sendo cuidadosamente pensados e em breve divulgaremos por aqui.</p>
		</div>
	</section>
	<section class="meio">
		<div class="col-1-2 mobile-one-whole fl" style="background-color: #E6E6E6; padding: 6% 0; box-sizing: border-box;">
			<div class="col-1-2 mobile-nine-tenths center">
				<div data-aos="fade-up" data-aos-duration="600">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 75.2 69.9" style="enable-background:new 0 0 75.2 69.9; height: 80px;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#BF2525;}
					</style>
					<g id="XMLID_269_">
						<path id="XMLID_274_" class="st0" d="M75.2,25C75.2,11.2,64,0,50.3,0H24.9C11.2,0,0,11.2,0,25v6.7c0,10.7,6.8,20.2,17,23.6l0.5,0.2
							l-0.3,0.4c-1.3,2.3-3.2,5.9-5.5,11.1c-0.4,0.8-0.2,1.7,0.4,2.3c0.6,0.6,1.6,0.8,2.3,0.4L43,56.6h7.3c13.8,0,24.9-11.2,24.9-25V25z
							 M71.1,31.7c0,11.5-9.3,20.9-20.8,20.9h-7.7c-0.3,0-0.6,0.1-0.9,0.2L17.8,63.6l0.5-1.1c1-2,2.8-5.4,3.6-6.5l0.1-0.1
							c0.5-0.3,0.9-0.9,1-1.5c0.1-1.1-0.6-2.1-1.7-2.3c-10-1.8-17.3-10.4-17.3-20.5V25c0-11.5,9.3-20.9,20.8-20.9h25.4
							c11.5,0,20.8,9.4,20.8,20.9V31.7z"/>
						<path id="XMLID_37_" class="st0" d="M18.5,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5s5-2.3,5-5S21.2,23.8,18.5,23.8z"/>
						<path id="XMLID_27_" class="st0" d="M56.8,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5c2.8,0,5-2.3,5-5S59.6,23.8,56.8,23.8z"/>
						<path id="XMLID_28_" class="st0" d="M37.6,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5c2.8,0,5-2.3,5-5S40.4,23.8,37.6,23.8z"/>
					</g>
					</svg>
				</div>
				<h1 style="font-size: 4em; margin: 5% 0;">BoldTalk</h1>
				<p style="font-size: 1.8em; line-height: 1.6em;">Gestão, inovação e cultura apresentados por mentores do mercado.</p>
			</div>
		</div>
		<div class="col-1-2 mobile-one-whole fl" style="background-color: #FFFFFF; padding: 6% 0; box-sizing: border-box;">
			<div class="col-1-2 mobile-nine-tenths center">
				<div data-aos="fade-up" data-aos-duration="600">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 79.2 79.2" style="enable-background:new 0 0 79.2 79.2; height: 80px;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#BF2525;}
					</style>
					<path id="XMLID_270_" class="st0" d="M79.2,39.6c0-4.5-8.3-7.2-18.4-8.8c1.3-1.8,2.5-3.5,3.5-5.1c4.5-7.1,5.6-11.9,3.3-14.1
						c-0.8-0.8-1.8-1.1-3.1-1.1c-3.5,0-9.3,3-16.1,8C46.9,8.3,44.1,0,39.6,0s-7.2,8.3-8.8,18.4c-6.8-5-12.6-8-16.1-8
						c-1.3,0-2.4,0.4-3.1,1.1c-3.2,3.2,0.8,11,6.9,19.3C8.4,32.4,0,35.2,0,39.6c0,4.5,8.3,7.2,18.4,8.8c-1.3,1.8-2.5,3.5-3.5,5.1
						c-4.5,7.1-5.6,11.9-3.3,14.1c0.8,0.8,1.8,1.1,3.1,1.1c3.5,0,9.3-3,16.1-8c1.5,10.1,4.3,18.4,8.8,18.4s7.2-8.3,8.8-18.4
						c6.8,5,12.6,8,16.1,8c1.3,0,2.4-0.4,3.1-1.1c2.3-2.3,1.1-7-3.3-14.1c-1-1.6-2.2-3.4-3.5-5.1C70.9,46.9,79.2,44.1,79.2,39.6z
						 M64.5,13.5c0.3,0,0.8,0,1,0.3c0.6,0.6,0.6,3.5-3.7,10.4c-1.2,2-2.7,4.1-4.4,6.3c-2.6-0.3-5.2-0.5-7.9-0.7c-0.2-2.6-0.4-5.3-0.7-7.9
						C56.1,16.3,61.8,13.5,64.5,13.5z M46.9,39.6c0,1,0,2,0,3c-0.7,0.7-1.4,1.4-2.1,2.1c-0.7,0.7-1.4,1.4-2.1,2.1c-1,0-2,0-3,0s-2,0-3,0
						c-0.7-0.7-1.4-1.4-2.1-2.1c-0.7-0.7-1.4-1.4-2.1-2.1c0-1,0-2,0-3c0-1,0-2,0-3c0.7-0.7,1.4-1.4,2.1-2.1c0.7-0.7,1.4-1.4,2.1-2.1
						c1,0,2,0,3,0c1,0,2,0,3,0c0.7,0.7,1.4,1.4,2.1,2.1c0.7,0.7,1.4,1.4,2.1,2.1C46.9,37.6,46.9,38.6,46.9,39.6z M43.9,29.4
						c-0.7-0.6-1.3-1.3-2-1.9c1.5-1.3,2.9-2.5,4.3-3.6c0.2,1.7,0.3,3.6,0.5,5.5C45.7,29.5,44.7,29.5,43.9,29.4z M35.4,29.4
						c-0.9,0-1.8,0.1-2.7,0.1c0.1-2,0.3-3.8,0.5-5.5c1.4,1.1,2.8,2.3,4.3,3.6C36.7,28.2,36.1,28.8,35.4,29.4z M29.4,35.4
						c-0.6,0.7-1.3,1.3-1.9,2c-1.3-1.5-2.5-2.9-3.6-4.3c1.8-0.2,3.6-0.3,5.6-0.5C29.5,33.6,29.5,34.5,29.4,35.4z M29.4,43.8
						c0,0.9,0.1,1.8,0.1,2.7c-2-0.1-3.8-0.3-5.6-0.5c1.1-1.4,2.3-2.8,3.6-4.3C28.2,42.5,28.8,43.2,29.4,43.8z M35.4,49.8
						c0.7,0.6,1.3,1.3,2,1.9c-1.5,1.3-2.9,2.5-4.3,3.6c-0.2-1.7-0.3-3.6-0.5-5.6C33.6,49.8,34.5,49.8,35.4,49.8z M43.9,49.8
						c0.9,0,1.8-0.1,2.7-0.1c-0.1,2-0.3,3.8-0.5,5.6c-1.4-1.1-2.8-2.3-4.3-3.6C42.5,51.1,43.2,50.4,43.9,49.8z M49.8,43.9
						c0.6-0.7,1.2-1.3,1.9-2c1.3,1.4,2.5,2.9,3.6,4.3c-1.7,0.2-3.6,0.3-5.6,0.5C49.8,45.7,49.8,44.7,49.8,43.9z M49.8,35.4
						c0-0.9-0.1-1.8-0.1-2.7c2,0.1,3.8,0.3,5.6,0.5c-1.1,1.4-2.3,2.8-3.6,4.3C51.1,36.7,50.4,36.1,49.8,35.4z M39.6,3
						c1.7,0,4.5,5.9,6.1,17.5c-2,1.5-4,3.2-6.1,5.1c-2.1-1.8-4.1-3.5-6.1-5.1C35.1,8.9,37.9,3,39.6,3z M13.7,13.7c0.2-0.2,0.7-0.3,1-0.3
						c2.7,0,8.4,2.9,15.7,8.4c-0.3,2.6-0.5,5.2-0.7,7.9c-2.6,0.2-5.3,0.4-7.9,0.7C14.7,21.1,12.5,15,13.7,13.7z M3,39.6
						c0-1.7,6-4.5,17.5-6.1c1.6,2.1,3.3,4.1,5.1,6.1c-1.8,2.1-3.5,4.1-5.1,6.1C8.9,44.1,3,41.4,3,39.6z M14.7,65.8c-0.3,0-0.8,0-1-0.3
						c-0.6-0.6-0.6-3.5,3.7-10.4c1.2-2,2.7-4.1,4.4-6.3c2.6,0.3,5.2,0.5,7.9,0.7c0.2,2.6,0.4,5.3,0.7,7.9C23.2,62.9,17.5,65.8,14.7,65.8z
						 M39.6,76.2c-1.7,0-4.5-5.9-6.1-17.5c2-1.5,4-3.2,6.1-5.1c2.1,1.8,4.1,3.5,6.1,5.1C44.1,70.3,41.4,76.2,39.6,76.2z M61.8,55.1
						c4.3,6.9,4.3,9.8,3.7,10.4c-0.2,0.2-0.7,0.3-1,0.3c-2.7,0-8.4-2.9-15.7-8.4c0.3-2.6,0.5-5.2,0.7-7.9c2.6-0.2,5.3-0.4,7.9-0.7
						C59.1,51,60.5,53.1,61.8,55.1z M58.8,45.7c-1.6-2-3.3-4-5.1-6.1c1.8-2.1,3.5-4.1,5.1-6.1c11.5,1.6,17.5,4.3,17.5,6.1
						C76.2,41.4,70.3,44.1,58.8,45.7z"/>
					</svg>
				</div>
				<h1 style="font-size: 4em; margin: 5% 0;">Lab</h1>
				<p style="font-size: 1.8em; line-height: 1.6em;">Cases de sucesso e estratégias de negócio em apresentações paralelas.</p>
			</div>
		</div>
	</section>
	<section class="meio">
		<div class="col-1-2 mobile-one-whole fl" style="background-color: #FFFFFF; padding: 6% 0; box-sizing: border-box;">
			<div class="col-1-2 mobile-nine-tenths center">
				<div data-aos="fade-up" data-aos-duration="600">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 71.4 87.9" style="enable-background:new 0 0 71.4 87.9; height: 80px;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#BF2525;}
					</style>
					<g id="XMLID_293_">
						<g id="XMLID_294_" transform="translate(-604.000000, -358.000000)">
							<g id="XMLID_295_" transform="translate(603.000000, 358.000000)">
								<path id="XMLID_304_" class="st0" d="M9.2,28.8C9.2,28.8,9.2,28.8,9.2,28.8l54.9,0c0,0,0,0,0,0v5.5c0,0,0,0,0,0H9.2c0,0,0,0,0,0
									V28.8L9.2,28.8z M3.7,28.8v5.5c0,3,2.5,5.5,5.5,5.5h54.9c3,0,5.5-2.5,5.5-5.5v-5.5c0-3-2.5-5.5-5.5-5.5H9.2
									C6.2,23.3,3.7,25.8,3.7,28.8L3.7,28.8z"/>
								<path id="XMLID_301_" class="st0" d="M17.5,39.8C17.5,39.8,17.5,39.8,17.5,39.8l38.4,0c0,0,0,0,0,0v42.5c0,0,0,0,0,0H17.5
									c0,0,0,0,0,0V39.8L17.5,39.8z M12,39.8v42.5c0,3,2.5,5.5,5.5,5.5h38.4c3,0,5.5-2.5,5.5-5.5V39.8c0-3-2.5-5.5-5.5-5.5H17.5
									C14.4,34.3,12,36.8,12,39.8L12,39.8z"/>
								<path id="XMLID_30_" class="st0" d="M69.7,82.4H3.7c-1.5,0-2.7,1.2-2.7,2.7c0,1.5,1.2,2.7,2.7,2.7h65.9c1.5,0,2.7-1.2,2.7-2.7
									C72.4,83.6,71.2,82.4,69.7,82.4L69.7,82.4z"/>
								<path id="XMLID_297_" class="st0" d="M31.2,5.5C31.2,5.5,31.2,5.5,31.2,5.5l11,0c0,0,0,0,0,0v2.7c0,0,0,0,0,0h-11c0,0,0,0,0,0
									V5.5L31.2,5.5z M25.7,5.5v2.8c0,3,2.5,5.5,5.5,5.5h11c3,0,5.5-2.5,5.5-5.5V5.5c0-3-2.5-5.5-5.5-5.5h-11C28.2,0,25.7,2.5,25.7,5.5
									L25.7,5.5z"/>
								<path id="XMLID_29_" class="st0" d="M17.5,15.1c0-3,2.5-5.5,5.5-5.5h4.1c1.5,0,2.7-1.2,2.7-2.7s-1.2-2.7-2.7-2.7H23
									c-6.1,0-11,4.9-11,11v9.6c0,1.5,1.2,2.7,2.7,2.7s2.7-1.2,2.7-2.7V15.1z"/>
							</g>
						</g>
					</g>
					</svg>
				</div>
				<h1 style="font-size: 4em; margin: 5% 0;">Keynotes</h1>
				<p style="font-size: 1.8em; line-height: 1.6em;">Palestras sobre temas diversos relacionados a trade marketing com especialistas do segmento.</p>
			</div>
		</div>
		<div class="col-1-2 mobile-one-whole fl" style="background-color: #E6E6E6; padding: 6% 0; box-sizing: border-box;">
			<div class="col-1-2 mobile-nine-tenths center">
				<div data-aos="fade-up" data-aos-duration="600">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 71.4 81.2" style="enable-background:new 0 0 71.4 81.2; height: 80px;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#BF2525;}
					</style>
					<path id="XMLID_296_" class="st0" d="M24.3,11.4C24.3,5.1,29.4,0,35.7,0s11.4,5.1,11.4,11.4S42,22.7,35.7,22.7S24.3,17.6,24.3,11.4z
						 M41.2,30.5c0.1-2.5,1-4.8,2.3-6.7c-1-0.2-2-0.3-3-0.3h-9.6c-1,0-2,0.1-3,0.3c1.5,2,2.3,4.5,2.3,7.2c1.8-1,3.8-1.5,6-1.5
						C38,29.4,39.6,29.8,41.2,30.5z M43.1,31.5c3.3,2.1,5.6,5.6,6,9.6c1.3,0.6,2.8,1,4.4,1c5.8,0,10.5-4.7,10.5-10.5s-4.7-10.5-10.5-10.5
						C47.8,21.2,43.2,25.8,43.1,31.5z M36.2,52.9c5.8,0,10.5-4.7,10.5-10.5S42,31.9,36.2,31.9s-10.5,4.7-10.5,10.5S30.4,52.9,36.2,52.9z
						 M40.7,53.6h-8.9c-7.4,0-13.4,6-13.4,13.4v10.9v0.2l0.7,0.2c7,2.2,13.2,2.9,18.2,2.9c9.8,0,15.5-2.8,15.9-3l0.7-0.4H54V67
						C54.1,59.6,48,53.6,40.7,53.6z M58,42.8h-8.8c-0.1,3.5-1.6,6.7-4,9c6.6,2,11.4,8,11.4,15.2v3.3c8.7-0.3,13.7-2.8,14-2.9l0.7-0.4h0.1
						V56.2C71.4,48.8,65.4,42.8,58,42.8z M17.9,42.1c2,0,3.9-0.6,5.6-1.6c0.5-3.3,2.3-6.3,4.9-8.3c0-0.2,0-0.4,0-0.6
						c0-5.8-4.7-10.5-10.5-10.5S7.4,25.8,7.4,31.6S12.1,42.1,17.9,42.1z M27.3,51.8c-2.4-2.3-3.9-5.4-4-8.9c-0.3,0-0.6,0-1,0h-8.9
						C6,42.9,0,48.9,0,56.3v10.9v0.2l0.7,0.2c5.7,1.8,10.7,2.6,15.1,2.8v-3.3C15.9,59.8,20.7,53.7,27.3,51.8z"/>
					</svg>
				</div>
				<h1 style="font-size: 4em; margin: 5% 0;">Painéis</h1>
				<p style="font-size: 1.8em; line-height: 1.6em;">Debate de ideias e temas relevantes feito por profissionais com participação aberta ao público.</p>
			</div>
		</div>
	</section>
	<section id="o-local" class="meio page page--dois">
		<div class="container">
			<div class="cont">
				<div class="local col-3-5 desk-seven-tenths tablet-nine-tenths mobile-one-whole center">
					<h1 class="title">O Local</h1>
					<div class="local__container">
						<div class="allimgs2 col-1-2 mobile-one-whole" data-aos="fade-up" data-aos-duration="600">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao0.jpg" alt="">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao1-1.jpg" alt="">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao2.jpg" alt="">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao3.jpg" alt="">
						</div>
						<div class="col-1-2 mobile-one-whole fl" style="padding-top: 8%;">
							<p><strong>Costão do</strong></p>
							<p class="bottom"><strong>Santinho</strong></p>
							<div class="col-1-2 mobile-one-whole fl local__content">
								<p>O melhor resort de praia do Brasil, para a melhor e maior experiência em trade marketing.</p>
								<p>Estrutura completa para um grande evento com apoio de hospedagem em um hotel referência no Brasil.</p>
							</div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao-resort-golf-spa-05.png" alt="" class="col-2-5 mobile-one-whole fl">
							<div class="clear"></div>
							<a href="http://www.costao.com.br/" target="_blank" class="btn btn--local mobile-nine-tenths">Saiba mais</a>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</section>
	<section id="pre-venda" style="background-color: #000;">
		<div class="container">
			<div class="col-3-5 tablet-three-quarters mobile-one-whole flex center">
				<div class="col-1-2 mobile-one-whole fl pre">
					<h1 class="title title--pre-venda">INSCRIÇÕES</h1>
					<p class="paragraph paragraph--pre-venda"><span style="text-decoration: line-through;">PRÉ-VENDA R$ 350,00</span> <span style="background-color: #fbbf42;color: #000;padding: 6px;border-radius: 4px;font-weight: 700;"><span style="transform: rotate(-6deg); display: inline-block;">Sold Out</span></span></p>
					<p class="paragraph paragraph--pre-venda mBottom"><i class="italic">1º LOTE  R$ 600,00</i><br></p>
					<a href="https://www.sympla.com.br/agile-experience17__117773" class="btn btn--action" target="_blank">Inscreva-se agora</a>
				</div>
				<div class="list col-1-2 mobile-one-whole fl">
					<ul class="list__container col-1-1">
						<li class="col-1-1 list__item">Kit evento</li>
						<li class="col-1-1 list__item">Coffee break</li>
						<li class="col-1-1 list__item">Feira de negócios</li>
						<li class="col-1-1 list__item">Happy hour com chopp artesanal</li>
						<li class="col-1-1 list__item">Palestras internacionais</li>
						<li class="col-1-1 list__item list__item--small">*Não inclui hospedagem</li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section id="hospedagem" class="meio" style="background-color: #a9120f">
		<div class="hospedagem col-4-5 tablet-nine-tenths desk-nine-tenths mobile-one-whole fr">
			<h1 class="title">Hospedagem</h1>
			<div class="col-1-1 fl" style="box-sizing: border-box; padding-left: 10%;">
				<!--div class="col-3-5 mobile-one-whole fl">
					<div class="col-7-10 mobile-one-whole fl hospedagem__content">
						<p>Para uma experiência completa recomendamos que você também se hospede no Costão do Santinho.</p>
						<p>Preços com <strong>30% de desconto</strong> para participantes do evento, incluindo todas as refeições durante o período de estadia e condição de pagamento <strong>em até 10x</strong>.</p>
						<p>Acesse e garanta sua hospedagem no melhor resort de praia do Brasil:</p>
						<div class="clear"></div>
						<a href="http://bit.ly/2kthO9L" target="_blank" class="btn btn-local mobile-nine-tenths" style="margin-left: 0; float: left; margin-top: 21%; margin-right: 5%;">Reservar</a>
						<img src="<php echo get_template_directory_uri(); ?>/assets/images/costao-resort-golf-spa-05.png" alt="" class="col-1-2 mobile-one-whole fl">
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div-->
				<!--div class="col-2-5 mobile-one-whole fr" style="width: 38%;">
					<img src="<php echo get_template_directory_uri(); ?>/assets/images/grid2.jpg" alt="" class="col-1-1 mobile-one-whole fl">
				</div-->
				<!--div class="clear"></div-->
				<div class="cards col-4-5 mobile-one-whole" style="margin-bottom: 8%;">
					<!--p style="padding: 0; margin-bottom: 6%;">Outras opções de hospedagem:</p-->
					<div class="cards__item col-3-10 mobile-one-whole fl">
						<h1 class="cards__title">Oceania Park Hotel</h1>
						<h2 class="cards__valor"><span style="font-size: .5em;">A partir de</span> R$ 252,00</h2>
						<ul class="cards__lista"  style="border-left: 1px solid #e6e6e6 !important;">
							<li class="cards__lis-item">Apartamento single</li>
							<li class="cards__lis-item">Café da manhã</li>
							<li style="padding: 10% 5% 5%;font-size: 1.6em;">A 5,4 km do Costão do Santinho</li>
							<li><a href="http://www.oceaniaparkhotel.com.br/" target="_blank" class="btn btn--local mobile-nine-tenths" style="margin-left: 0;width: 90%; margin-left: 5%">Faça sua reserva</a></li>
						</ul>
					</div>
					<div class="cards__item-plus cards__item-plus--dest mobile-one-whole fl">
						<div class="cards__tag">OFICIAL AEx</div>
						<h1 class="cards__title-plus">Costão do Santinho</h1>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/DSC07175.jpg" alt="" style="width: 100%;">
						<h2 class="cards__valor-plus"><span style="font-size: .5em;">A partir de</span> R$ 571,00<br><span style="font-size: .6em;">30% de desconto e em até 10 vezes</span></h2>
						<ul class="cards__lista-plus">
							<li style="padding: 6% 5% 3%;font-size: 1.6em;border-bottom: 2px solid #e6e6e6;">Apartamento single</li>
							<li class="cards__lis-item-plus">Pensão completa de alimentação com bebidas não alcoólicas:
								<ul style="text-align: left; font-size: .8em; padding: 2.5% 0 0 10%; list-style: square;">
									<li style="padding: 1.5% 0;">Café da manhã</li>
									<li style="padding: 1.5% 0;">Almoço gourmet livre</li>
									<li style="padding: 1.5% 0;">Jantar (gastronomia italiana, japonesa e contemporânea)</li>
								</ul>
							</li>
							<li class="cards__lis-item-plus">Trilhas arqueológicas guiadas</li>
							<li class="cards__lis-item-plus">11 piscinas (uma aquecida)</li>
							<li style="padding: 3% 5% 0;font-size: 1.6em;">Acesso direto à praia</li>
							<li><a href="http://bit.ly/2kthO9L" target="_blank" class="btn btn--local mobile-nine-tenths" style="margin-left: 0;width: 90%; margin-left: 5%">Faça sua reserva</a></li>
						</ul>
					</div>
					<div class="cards__item col-3-10 mobile-one-whole fl">
						<h1 class="cards__title">Slaviero Hotéis</h1>
						<h2 class="cards__valor"><span style="font-size: .5em;">A partir de</span> R$ 345,00</h2>
						<ul class="cards__lista" style="border-right: 1px solid #e6e6e6 !important;">
							<li class="cards__lis-item">Apartamento single</li>
							<li class="cards__lis-item">Café da manhã</li>
							<li class="cards__lis-item">Diária estacionamento R$ 30,65</li>
							<li style="padding: 10% 5% 5%;font-size: 1.6em;">A 5,7 km do Costão do Santinho</li>
							<li><a href="http://www.slavierohoteis.com.br/hotel-em-florianopolis/slaviero-essential-florianopolis-ingleses-acquamar/hotel/" target="_blank" class="btn btn--local mobile-nine-tenths" style="margin-left: 0;width: 90%; margin-left: 5%">Faça sua reserva</a></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</section>
	<section style="background-color: #FFF">
		<div class="hospedagem col-4-5 tablet-nine-tenths desk-nine-tenths mobile-one-whole center" style="padding-bottom: 5%;">
			<h1 class="title">FAQ</h1>
			<div class="fl col-1-2 mobile-one-whole ">
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Vocês emitem certificado online?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		              <div class="tab__paragraph">Sim. Os certificados serão enviados via e-mail pela equipe organizadora em até uma semana após o evento.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Como pagar a minha inscrição?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		              <div class="tab__paragraph">Todas as inscrições podem ser compradas por boleto bancário, débito online e cartão de crédito (10x).</div>
		              <div class="tab__paragraph">Os pagamentos realizados do exterior somente podem ser processados em uma parcela e em moeda local.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Vocês emitem nota fiscal?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		              <div class="tab__paragraph">Sim. Uma vez confirmado o pagamento, sua nota fiscal seguirá em até duas semanas subsequente à data de compra para o e-mail informado no momento do cadastro. Mais informações <a href="mailto:financeiro@involves.com.br" style="color: #a9120f;">financeiro@involves.com.br</a>.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Incide algum imposto na minha compra? Qual?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">“Ressaltamos que esta nota fiscal contempla o serviço 12.08 de feiras, exposições, eventos e congressos. Portanto, o tomador do serviço não deve realizar retenções Federais e/ou Municipais. Legislação: IRRF RIR/1999, PIS/COFINS/CSLL Lei Nº 10.833/2003 e ISS Lei Complementar 116/2003”</div><div class="tab__paragraph">Uma vez confirmado o pagamento, sua nota fiscal seguirá em até duas semanas após a compra para o e-mail informado no momento do cadastro.</div>
<div class="tab__paragraph">SERÁ EMITIDA SOMENTE UMA NOTA FISCAL POR PEDIDO.</div>
<div class="tab__paragraph">Mais informações <a href="mailto:financeiro@involves.com.br" style="color: #a9120f;">financeiro@involves.com.br</a>.</div>
					</div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Como funcionará a inscrição no evento?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		              	<div class="tab__paragraph">Nosso credenciamento estará funcionando meia hora antes da primeira palestra, no local do evento. Chegue cedo e evite filas. Não esqueça de trazer a impressão do ingresso recebido por e-mail. É indispensável a apresentação de documento de identidade com foto.</div>
		            	<div class="tab__paragraph">Os participantes do Agile Experience estão cientes e concordam que fotografias e filmagens feitas durante o evento poderão ser utilizadas pelos organizadores para promoção e divulgação do mesmo.</div>
		            	<div class="tab__paragraph">Mais informações: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a></div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">A hospedagem no Costão do Santinho está inclusa no ingresso?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Não, mas são oferecidos descontos para participantes do evento.</div>
		            </div>
				</div>
			</div>
			<div class="fl col-1-2 mobile-one-whole ">
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Existe algum desconto para a compra em grupo?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Sim! Se você ou sua empresa realizarem a compra de duas ou mais inscrições está garantindo um desconto de 10%. Exceto para os ingressos de pré-vendas. </div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Não recebeu o seu e-mail de confirmação?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Depois de fazer login em sua conta na <a href="https://www.sympla.com.br/agile-experience17__117773" target="_blank" style="color: #a9120f;">Sympla</a>, acesse o menu da conta e selecione Ingressos. Caso não tenha recebido a confirmação da sua compra em até 48h, entre em contato pelo e-mail: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Posso cancelar a minha inscrição?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Sim. Cancelamentos serão aceitos até 07 (sete) dias corridos após a compra do ingresso, desde que 48h antes do evento, sem exceção. Caso o ingresso tenha sido comprado pela opção da compra em grupo, o cancelamento de uma inscrição cancela todo o pacote do grupo, tendo que ser refeito o processo de compra.</div>
		            	<div class="tab__paragraph">Dúvidas? Entre em contato através: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Posso transferir a minha inscrição?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Transferência de titularidade será aceita desde que 48h antes da data do evento. Entre em contato através do e-mail: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div style="width: 100%; float: left;">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl" style="margin-top: 1%;padding-left: 0;margin: 6% 0 0;">
						<div style="padding: 2% 6% 2% 2%;font-size: .15em;">Como faço para patrocinar o AEx2017?</div>
						<span class="tab__btn tab__btn--open" style="font-size: .2em;">+</span>
						<span class="tab__btn tab__btn--close" style="font-size: .2em;">-</span>
					</h1>
		            <div class="tab__item">
		            	<div class="tab__paragraph">Para mais informações sobre as cotas de patrocínio envie um e-mail para <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section id="contato" class="contato">
		<div class="container">
			<span class="alert alert--error">Por favor preencha todos os campos!</span>
    		<span class="alert alert--success">Seus dados foram enviados.</span>
			<div class="flex col-4-5 mobile-nine-tenths center contato__container">
				<div class="col-1-2 mobile-one-whole fl">
					<h1 class="title">Contato</h1>
					<h2 class="contato__title">Fale com a gente para saber mais sobre o evento.</h2>
				</div>
				<div class="col-1-2 mobile-one-whole rl">
					<form class="form" id="formulario">
						<input type="text" placeholder="Nome" id="nome" name="nome" class="form__field">
						<input type="text" placeholder="Empresa" id="empresa" name="empresa" class="form__field">
						<input type="text" placeholder="E-mail" id="email" name="email" class="form__field">
						<input type="text" placeholder="Telefone" id="telefone" name="telefone" class="form__field">
						<textarea name="mensagem" id="mensagem" placeholder="Mensagem" class="form__field form__field--message"></textarea>
						<input type="submit" class="btn btn--submit" value="Enviar">
					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<footer class="footer">
		<div class="container">
			<div class="col-4-5 flex center">
				<div class="footer__info col-1-2 mobile-one-whole flex">
					<p>Uma iniciativa</p>
					<a href="http://www.involves.com.br/" target="_blank" class="footer__logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-involves.svg" alt="" style="height: 34px;"></a>
				</div>

				<div class="footer__cont col-1-2 mobile-one-whole">
	    			<p class="fr">+55 11 4063.6633  -  <a href="mailto:marketing@involves.com.br">marketing@involves.com.br</a></p>
	    		</div>
	    		<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>