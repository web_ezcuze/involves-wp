<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Agile Experience | A Maior Experiência em Trade Marketing</title>
	
	<meta name="description" content="Description">
	<meta name="keywords" content="Keywords">
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<meta name="robots" content="noindex,follow"/>
	<link rel="canonical" href="https://agilexperience.com.br" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="object" />
	<meta property="og:title" content="2017 | Agile Experience" />
	<meta property="og:url" content="https://agilexperience.com.br" />
	<meta property="og:site_name" content="Agile Experience" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="2017 | Agile Experience" />
	<?php wp_head(); ?>
</head>
<body>
	<section id="home" class="parallax">
		<nav class="nav2">
			<a href="#aex-2017" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-aex2017.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">AEx 2017</span></a>
			<a href="#programacao" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-programacao.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Programação</span></a>
			<a href="#palestrantes" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-palestrantes.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Palestrantes</span></a>
			<a href="#o-local" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-local.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">O Local</span></a>
			<a href="#hospedagem" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hospedagem.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Hospedagem</span></a>
			<a href="#aliste-se" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-inscricoes.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Aliste-se</span></a>
			<a href="#faq" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-faq.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">FAQ</span></a>
			<a href="#contato" class="nav2__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-contato.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Contato</span></a>
			<a href="https://www.facebook.com/pg/involves/photos/?tab=album&album_id=1131607366886328" target="_blank" class="nav2__link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-2016.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Edição 2016</span></a>
			<a href="https://www.sympla.com.br/aex-pocket-rio__133339" target="_blank" class="nav2__link" style="border-bottom: none;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pocket.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">AEx Pocket</span></a>
		</nav>
		<header class="header header--inner">
			<div class="container center" style="float: inherit; display: block;">
				<a href="#home" class="logo-white scroll">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
					<style type="text/css">
						.sta0{fill:#FFF !important;}
						.sta1{fill:#FFF !important;}
					</style>
					<g id="XMLID_12_">
						<path id="XMLID_17_" class="sta0" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
							L26.3,10.8z"/>
						<path id="XMLID_15_" class="sta0" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
						<path id="XMLID_13_" class="sta1" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
							L122,29.6H114.5z"/>
					</g>
					</svg>
				</a>
				<a href="#home" class="logo-red scroll">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 122 50.6" style="enable-background:new 0 0 122 50.6;" xml:space="preserve">
					<style type="text/css">
						.sta2{fill:#FFF !important;}
						.sta3{fill:#BC2026 !important;}
					</style>
					<g id="XMLID_12_">
						<path id="XMLID_17_" class="sta2" d="M40.3,50.6L37.2,42H15.5l-3.2,8.6H0L19.6,0h13.5l19.5,50.6H40.3z M26.3,10.8l-7.9,21.8h15.8
							L26.3,10.8z"/>
						<path id="XMLID_15_" class="sta2" d="M56.8,50.6V0h35.8v9.5h-25v10.7h24.5v9.5H67.6v11.5h25v9.5H56.8z"/>
						<path id="XMLID_13_" class="sta3" d="M114.5,29.6L107,18.8l-7.5,10.8h-7.5l10.7-15.2L92.7,0h7.5l6.8,10.1L113.7,0h7.6l-10.1,14.4
							L122,29.6H114.5z"/>
					</g>
					</svg>
				</a>
			</div>
			<nav class="nav">
					<a href="#aex-2017" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-aex2017.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">AEx 2017</span><div class="clear"></div></a>
					<a href="#programacao" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-programacao.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Programação</span><div class="clear"></div></a>
					<a href="#palestrantes" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-palestrantes.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Palestrantes</span><div class="clear"></div></a>
					<a href="#o-local" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-local.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">O Local</span><div class="clear"></div></a>
					<a href="#hospedagem" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hospedagem.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Hospedagem</span><div class="clear"></div></a>
					<a href="#aliste-se" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-inscricoes.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Aliste-se</span><div class="clear"></div></a>
					<a href="#faq" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-faq.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">FAQ</span><div class="clear"></div></a>
					<a href="#contato" class="nav__link scroll"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-contato.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Contato</span><div class="clear"></div></a>
					<a href="https://www.facebook.com/pg/involves/photos/?tab=album&album_id=1131607366886328" target="_blank" class="nav__link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-2016.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">Edição 2016</span><div class="clear"></div></a>
					<a href="https://www.sympla.com.br/aex-pocket-rio__133339" target="_blank" class="nav__link" style="border-bottom: none;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pocket.png" alt="" class="fl" style="width: 25px;margin-top: -10px;margin-right: 10px;"><span class="fl">AEx Pocket</span><div class="clear"></div></a>
					<div id="hamburger" class="hamburger">
				      <div class="hamburger__all">
				        <hr class="line_1">
				        <hr class="line_2">
				        <hr class="line_3">
				      </div>
				    </div>
				</nav>
		</header>
		<div class="slick--parallax">
			<div>

				<div class="js-perspective" style="width: 100%; height: 100%; overflow: hidden; display: inherit; margin: 0; padding: 0;">
					<video autoplay="autoplay" loop="loop" muted="" width="300" height="150"	 class="videofull" id="my-video">
						<source src="<?php echo get_template_directory_uri(); ?>/assets/video/AEx-2016.mp4" type="video/mp4">
					</video>
					<div class="">
						<div class="">
							<div class="">
								<span style="position: absolute;top: 50%;left: 50%;z-index: 999;-webkit-transform: translatey(-70%) translatex(-50%);-moz-transform: translatey(-70%) translatex(-50%);-o-transform: translatey(-70%) translatex(-50%);transform: translatey(-70%) translatex(-50%);text-shadow: 1px 1px 8px rgba(0,0,0,1);font-size: 3em;" class="conteudo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/batalhadopdv-slider.png" alt="" style="margin: 0 auto; width: 70%;font-family: 'Montserrat', sans-serif;"><br>21 e 22 de setembro no Costão do Santinho Resort, em Florianópolis</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="z3 tag">
			<h1>INSCRIÇÕES</h1><br>
			<span><span style="text-decoration: line-through;display: inline-block;font-size: .6em;">PRÉ-VENDA R$ 350,00</span> <span style="background-color: #fbbf42;color: #000;padding: 6px;border-radius: 4px;font-weight: 700;display: inline-block;font-size: 1em;"><span style="transform: rotate(-6deg);display: inline-block;font-size: .6em;">Sold Out</span></span></span>
			<span style="font-size: 2em; margin-top: 2.5%;"><i class="italic">1º LOTE  R$ 600,00</i></span>
			<a href="https://www.sympla.com.br/agile-experience17__117773" target="_blank" class="btn btn--home">Compre agora</a>
		</div>
	</section>
	<section id="aex-2017" class="aex-2017 page page--um no-overflow z2" data-aos="fade-up" data-aos-duration="600">
		<div  class="mobile-one-whole col-1-1" style="/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+50,ffffff+50&0+50,1+51 */
background: -moz-linear-gradient(left,  rgba(255,255,255,0) 50%, rgba(255,255,255,1) 51%); /* FF3.6-15 */
background: -webkit-linear-gradient(left,  rgba(255,255,255,0) 50%,rgba(255,255,255,1) 51%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right,  rgba(255,255,255,0) 50%,rgba(255,255,255,1) 51%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=1 ); /* IE6-9 */
">
			<div class="container">
				<div class="mobile-one-whole col-1-1 fl content" style="background-color: #FFF">
					<h1 class="title col-3-5"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-aex2017.svg" alt="" style="width: 60px;float: left;margin-left: -70px;margin-top: -20px;"></div>AEx 2017</h1>
					<h2 class="sub-title col-3-5">Único. Completo. Inovador.</h2>
					<p class="paragraph mobile-one-whole col-3-5">A maior experiência em trade marketing do Brasil: a proposta do <b class="bold">Agile Experience</b> é reunir profissionais de trade marketing para discutir desafios e práticas do segmento. O evento oferece palestras, networking e tradução simultânea.</p>
					<p class="paragraph mobile-one-whole col-3-5">A terceira edição, em 2017, acontecerá nos dias 21 e 22 de setembro, no Costão do Santinho Resort, em Florianópolis. Uma oportunidade imperdível e já consolidada como referência no universo do trade marketing.</p>
					<p class="paragraph mobile-one-whole col-3-5" style="font-weight: 700;">As melhores estratégias para execuções perfeitas te esperam aqui.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div  class="mobile-one-whole col-1-1" style="/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+50,eaeaea+50&0+50,1+51 */
background: -moz-linear-gradient(left,  rgba(234,234,234,0) 50%, rgba(234,234,234,1) 51%); /* FF3.6-15 */
background: -webkit-linear-gradient(left,  rgba(234,234,234,0) 50%,rgba(234,234,234,1) 51%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right,  rgba(234,234,234,0) 50%,rgba(234,234,234,1) 51%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00eaeaea', endColorstr='#eaeaea',GradientType=1 ); /* IE6-9 */
">
			<div class="container">
				<div class="mobile-one-whole col-1-1 fl content" style="background-color: #EAEAEA">
					<div class="mobile-one-whole col-1-1 fl">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/aex.png" class="mobile-one-whole fl img-aex">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/batalhadopdv.png" class="mobile-one-whole col-3-10 fl img-batalha"></div>
					<div class="clear"></div>
					<p class="paragraph mobile-one-whole col-3-5" style="margin-top: -2%">Criamos um conceito diferente para deixar nosso evento ainda mais interessante. Os desafios do Trade Marketing envolvem estratégia, comando, conquista e uma série de outras competências. Por isso escolhemos a Batalha no PDV como o tema para toda a programação que está sendo criada.</p>
					<p class="paragraph mobile-one-whole col-3-5">Você vai se surpreender com o AEx 2017. Corre lá no final da página e já faça seu alistamento com os melhores combatentes do mercado.</p>
					<p class="paragraph mobile-one-whole col-3-5">Esse é o seu segmento, é o nosso, é o Trade.</p>
					<p class="paragraph mobile-one-whole col-3-5">Venha fazer parte!</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section>
		<div class="mobile-one-whole col-1-1 fl">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img1.jpg" alt="" class="mobile-one-whole col-1-3 fl">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img2.jpg" alt="" class="mobile-one-whole col-1-3 fl">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img3.jpg" alt="" class="mobile-one-whole col-1-3 fl">
		</div>
	</section>
	<section class="info">
		<div class="container flex center">
			<article class="info__item mobile-one-whole col-1-3 fl">
				<div class="mobile-four-fifths center" style="justify-content: space-between;">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon1.svg" class="info__img" alt="Conteúdo" data-aos="fade-up" data-aos-duration="600">
					<div class="fl mobile-one-whole" style="align-self: center;">
						<h1>Conteúdo</h1>
						<p>Nomes importantes do trade marketing apresentam tendências, cases e desafios do mercado.</p>
					</div>
					<div class="clear"></div>
				</div>
			</article>
			<article class="info__item mobile-one-whole col-1-3 fl">
				<div class="mobile-four-fifths center" style="justify-content: space-between;">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon2.svg" class="info__img" alt="Networking" data-aos="fade-up" data-aos-duration="600">
					<div class="fl mobile-one-whole" style="align-self: center;">
						<h1>Networking</h1>
						<p>1.000 profissionais de trade marketing reunidos em um só lugar para troca de experiências e negócios.</p>
					</div>
					<div class="clear"></div>
				</div>
			</article>
			<article class="info__item mobile-one-whole col-1-3 fl">
				<div class="mobile-four-fifths center" style="justify-content: space-between;">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon3.svg" class="info__img" alt="Feira de Negócios" data-aos="fade-up" data-aos-duration="600">
					<div class="fl mobile-one-whole" style="align-self: center;">
						<h1>Feira de Negócios</h1>
						<p>Marcas e profissionais reunidos em um mesmo ambiente para gerar boas ideias e ótimos negócios.</p>
					</div>
					<div class="clear"></div>
				</div>
			</article>
		</div>
	</section>
	<section class="bar">
		Supreenda-se com o AEx 2016 e <a href="#pre-venda" class="scroll">garanta sua vaga</a> nessa edição. 
	</section>
	<section class="video">
		<div class="container">
			<div class="video__container">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/cH8I75zZrGA" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
	<section id="programacao" class="programacao" style="background-color: #EAEAEA;">
		<div class="container center">
			<h1 style="margin: 4% 0; margin-left: 10%;" class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-programacao.svg" alt="" style="width: 60px;float: left;margin-left: -70px;margin-top: -20px;"></div>Programação</h1>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cantil.png" alt="" style="width: 50%;position: absolute;right: -46%;top: -30%;z-index: 999;">
			<div class="col-1-4 mobile-one-whole fl programacao__item">
				<div class="col-1-2 mobile-one-whole center fl">
					<div data-aos="fade-up" data-aos-duration="600">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 75.2 69.9" style="enable-background:new 0 0 75.2 69.9; height: 80px;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#6C1211;}
						</style>
						<g id="XMLID_269_">
							<path id="XMLID_274_" class="st0" d="M75.2,25C75.2,11.2,64,0,50.3,0H24.9C11.2,0,0,11.2,0,25v6.7c0,10.7,6.8,20.2,17,23.6l0.5,0.2
								l-0.3,0.4c-1.3,2.3-3.2,5.9-5.5,11.1c-0.4,0.8-0.2,1.7,0.4,2.3c0.6,0.6,1.6,0.8,2.3,0.4L43,56.6h7.3c13.8,0,24.9-11.2,24.9-25V25z
								 M71.1,31.7c0,11.5-9.3,20.9-20.8,20.9h-7.7c-0.3,0-0.6,0.1-0.9,0.2L17.8,63.6l0.5-1.1c1-2,2.8-5.4,3.6-6.5l0.1-0.1
								c0.5-0.3,0.9-0.9,1-1.5c0.1-1.1-0.6-2.1-1.7-2.3c-10-1.8-17.3-10.4-17.3-20.5V25c0-11.5,9.3-20.9,20.8-20.9h25.4
								c11.5,0,20.8,9.4,20.8,20.9V31.7z"/>
							<path id="XMLID_37_" class="st0" d="M18.5,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5s5-2.3,5-5S21.2,23.8,18.5,23.8z"/>
							<path id="XMLID_27_" class="st0" d="M56.8,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5c2.8,0,5-2.3,5-5S59.6,23.8,56.8,23.8z"/>
							<path id="XMLID_28_" class="st0" d="M37.6,23.8c-2.8,0-5,2.3-5,5s2.3,5,5,5c2.8,0,5-2.3,5-5S40.4,23.8,37.6,23.8z"/>
						</g>
						</svg>
					</div>
					<h1 style="font-size: 4em; margin: 5% 0;">BoldTalk</h1>
					<p style="font-size: 1.8em; line-height: 1.6em;">Gestão, inovação e cultura apresentados por mentores do mercado.</p>
				</div>
			</div>
			<div class="col-1-4 mobile-one-whole fl programacao__item">
				<div class="col-1-2 mobile-one-whole center fl">
					<div data-aos="fade-up" data-aos-duration="600">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 79.2 79.2" style="enable-background:new 0 0 79.2 79.2; height: 80px;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#6C1211;}
						</style>
						<path id="XMLID_270_" class="st0" d="M79.2,39.6c0-4.5-8.3-7.2-18.4-8.8c1.3-1.8,2.5-3.5,3.5-5.1c4.5-7.1,5.6-11.9,3.3-14.1
							c-0.8-0.8-1.8-1.1-3.1-1.1c-3.5,0-9.3,3-16.1,8C46.9,8.3,44.1,0,39.6,0s-7.2,8.3-8.8,18.4c-6.8-5-12.6-8-16.1-8
							c-1.3,0-2.4,0.4-3.1,1.1c-3.2,3.2,0.8,11,6.9,19.3C8.4,32.4,0,35.2,0,39.6c0,4.5,8.3,7.2,18.4,8.8c-1.3,1.8-2.5,3.5-3.5,5.1
							c-4.5,7.1-5.6,11.9-3.3,14.1c0.8,0.8,1.8,1.1,3.1,1.1c3.5,0,9.3-3,16.1-8c1.5,10.1,4.3,18.4,8.8,18.4s7.2-8.3,8.8-18.4
							c6.8,5,12.6,8,16.1,8c1.3,0,2.4-0.4,3.1-1.1c2.3-2.3,1.1-7-3.3-14.1c-1-1.6-2.2-3.4-3.5-5.1C70.9,46.9,79.2,44.1,79.2,39.6z
							 M64.5,13.5c0.3,0,0.8,0,1,0.3c0.6,0.6,0.6,3.5-3.7,10.4c-1.2,2-2.7,4.1-4.4,6.3c-2.6-0.3-5.2-0.5-7.9-0.7c-0.2-2.6-0.4-5.3-0.7-7.9
							C56.1,16.3,61.8,13.5,64.5,13.5z M46.9,39.6c0,1,0,2,0,3c-0.7,0.7-1.4,1.4-2.1,2.1c-0.7,0.7-1.4,1.4-2.1,2.1c-1,0-2,0-3,0s-2,0-3,0
							c-0.7-0.7-1.4-1.4-2.1-2.1c-0.7-0.7-1.4-1.4-2.1-2.1c0-1,0-2,0-3c0-1,0-2,0-3c0.7-0.7,1.4-1.4,2.1-2.1c0.7-0.7,1.4-1.4,2.1-2.1
							c1,0,2,0,3,0c1,0,2,0,3,0c0.7,0.7,1.4,1.4,2.1,2.1c0.7,0.7,1.4,1.4,2.1,2.1C46.9,37.6,46.9,38.6,46.9,39.6z M43.9,29.4
							c-0.7-0.6-1.3-1.3-2-1.9c1.5-1.3,2.9-2.5,4.3-3.6c0.2,1.7,0.3,3.6,0.5,5.5C45.7,29.5,44.7,29.5,43.9,29.4z M35.4,29.4
							c-0.9,0-1.8,0.1-2.7,0.1c0.1-2,0.3-3.8,0.5-5.5c1.4,1.1,2.8,2.3,4.3,3.6C36.7,28.2,36.1,28.8,35.4,29.4z M29.4,35.4
							c-0.6,0.7-1.3,1.3-1.9,2c-1.3-1.5-2.5-2.9-3.6-4.3c1.8-0.2,3.6-0.3,5.6-0.5C29.5,33.6,29.5,34.5,29.4,35.4z M29.4,43.8
							c0,0.9,0.1,1.8,0.1,2.7c-2-0.1-3.8-0.3-5.6-0.5c1.1-1.4,2.3-2.8,3.6-4.3C28.2,42.5,28.8,43.2,29.4,43.8z M35.4,49.8
							c0.7,0.6,1.3,1.3,2,1.9c-1.5,1.3-2.9,2.5-4.3,3.6c-0.2-1.7-0.3-3.6-0.5-5.6C33.6,49.8,34.5,49.8,35.4,49.8z M43.9,49.8
							c0.9,0,1.8-0.1,2.7-0.1c-0.1,2-0.3,3.8-0.5,5.6c-1.4-1.1-2.8-2.3-4.3-3.6C42.5,51.1,43.2,50.4,43.9,49.8z M49.8,43.9
							c0.6-0.7,1.2-1.3,1.9-2c1.3,1.4,2.5,2.9,3.6,4.3c-1.7,0.2-3.6,0.3-5.6,0.5C49.8,45.7,49.8,44.7,49.8,43.9z M49.8,35.4
							c0-0.9-0.1-1.8-0.1-2.7c2,0.1,3.8,0.3,5.6,0.5c-1.1,1.4-2.3,2.8-3.6,4.3C51.1,36.7,50.4,36.1,49.8,35.4z M39.6,3
							c1.7,0,4.5,5.9,6.1,17.5c-2,1.5-4,3.2-6.1,5.1c-2.1-1.8-4.1-3.5-6.1-5.1C35.1,8.9,37.9,3,39.6,3z M13.7,13.7c0.2-0.2,0.7-0.3,1-0.3
							c2.7,0,8.4,2.9,15.7,8.4c-0.3,2.6-0.5,5.2-0.7,7.9c-2.6,0.2-5.3,0.4-7.9,0.7C14.7,21.1,12.5,15,13.7,13.7z M3,39.6
							c0-1.7,6-4.5,17.5-6.1c1.6,2.1,3.3,4.1,5.1,6.1c-1.8,2.1-3.5,4.1-5.1,6.1C8.9,44.1,3,41.4,3,39.6z M14.7,65.8c-0.3,0-0.8,0-1-0.3
							c-0.6-0.6-0.6-3.5,3.7-10.4c1.2-2,2.7-4.1,4.4-6.3c2.6,0.3,5.2,0.5,7.9,0.7c0.2,2.6,0.4,5.3,0.7,7.9C23.2,62.9,17.5,65.8,14.7,65.8z
							 M39.6,76.2c-1.7,0-4.5-5.9-6.1-17.5c2-1.5,4-3.2,6.1-5.1c2.1,1.8,4.1,3.5,6.1,5.1C44.1,70.3,41.4,76.2,39.6,76.2z M61.8,55.1
							c4.3,6.9,4.3,9.8,3.7,10.4c-0.2,0.2-0.7,0.3-1,0.3c-2.7,0-8.4-2.9-15.7-8.4c0.3-2.6,0.5-5.2,0.7-7.9c2.6-0.2,5.3-0.4,7.9-0.7
							C59.1,51,60.5,53.1,61.8,55.1z M58.8,45.7c-1.6-2-3.3-4-5.1-6.1c1.8-2.1,3.5-4.1,5.1-6.1c11.5,1.6,17.5,4.3,17.5,6.1
							C76.2,41.4,70.3,44.1,58.8,45.7z"/>
						</svg>
					</div>
					<h1 style="font-size: 4em; margin: 5% 0;">Lab</h1>
					<p style="font-size: 1.8em; line-height: 1.6em;">Cases de sucesso e estratégias de negócio em apresentações paralelas.</p>
				</div>
			</div>
			<div class="col-1-4 mobile-one-whole fl programacao__item">
				<div class="col-1-2 mobile-one-whole center fl">
					<div data-aos="fade-up" data-aos-duration="600">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 71.4 87.9" style="enable-background:new 0 0 71.4 87.9; height: 80px;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#6C1211;}
						</style>
						<g id="XMLID_293_">
							<g id="XMLID_294_" transform="translate(-604.000000, -358.000000)">
								<g id="XMLID_295_" transform="translate(603.000000, 358.000000)">
									<path id="XMLID_304_" class="st0" d="M9.2,28.8C9.2,28.8,9.2,28.8,9.2,28.8l54.9,0c0,0,0,0,0,0v5.5c0,0,0,0,0,0H9.2c0,0,0,0,0,0
										V28.8L9.2,28.8z M3.7,28.8v5.5c0,3,2.5,5.5,5.5,5.5h54.9c3,0,5.5-2.5,5.5-5.5v-5.5c0-3-2.5-5.5-5.5-5.5H9.2
										C6.2,23.3,3.7,25.8,3.7,28.8L3.7,28.8z"/>
									<path id="XMLID_301_" class="st0" d="M17.5,39.8C17.5,39.8,17.5,39.8,17.5,39.8l38.4,0c0,0,0,0,0,0v42.5c0,0,0,0,0,0H17.5
										c0,0,0,0,0,0V39.8L17.5,39.8z M12,39.8v42.5c0,3,2.5,5.5,5.5,5.5h38.4c3,0,5.5-2.5,5.5-5.5V39.8c0-3-2.5-5.5-5.5-5.5H17.5
										C14.4,34.3,12,36.8,12,39.8L12,39.8z"/>
									<path id="XMLID_30_" class="st0" d="M69.7,82.4H3.7c-1.5,0-2.7,1.2-2.7,2.7c0,1.5,1.2,2.7,2.7,2.7h65.9c1.5,0,2.7-1.2,2.7-2.7
										C72.4,83.6,71.2,82.4,69.7,82.4L69.7,82.4z"/>
									<path id="XMLID_297_" class="st0" d="M31.2,5.5C31.2,5.5,31.2,5.5,31.2,5.5l11,0c0,0,0,0,0,0v2.7c0,0,0,0,0,0h-11c0,0,0,0,0,0
										V5.5L31.2,5.5z M25.7,5.5v2.8c0,3,2.5,5.5,5.5,5.5h11c3,0,5.5-2.5,5.5-5.5V5.5c0-3-2.5-5.5-5.5-5.5h-11C28.2,0,25.7,2.5,25.7,5.5
										L25.7,5.5z"/>
									<path id="XMLID_29_" class="st0" d="M17.5,15.1c0-3,2.5-5.5,5.5-5.5h4.1c1.5,0,2.7-1.2,2.7-2.7s-1.2-2.7-2.7-2.7H23
										c-6.1,0-11,4.9-11,11v9.6c0,1.5,1.2,2.7,2.7,2.7s2.7-1.2,2.7-2.7V15.1z"/>
								</g>
							</g>
						</g>
						</svg>
					</div>
					<h1 style="font-size: 4em; margin: 5% 0;">Keynotes</h1>
					<p style="font-size: 1.8em; line-height: 1.6em;">Palestras sobre temas diversos relacionados a trade marketing com especialistas do segmento.</p>
				</div>
			</div>
			<div class="col-1-4 mobile-one-whole fl programacao__item">
				<div class="col-1-2 mobile-one-whole center fl">
					<div data-aos="fade-up" data-aos-duration="600">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 71.4 81.2" style="enable-background:new 0 0 71.4 81.2;height: 80px;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#6C1211;}
						</style>
						<path id="XMLID_296_" class="st0" d="M35.7,4c4.1,0,7.4,3.3,7.4,7.4c0,4.1-3.3,7.3-7.4,7.3c-4.1,0-7.4-3.3-7.4-7.3
							C28.3,7.3,31.6,4,35.7,4 M17.9,25.1c3.2,0,5.9,2.3,6.4,5.4c-2,2-3.5,4.5-4.3,7.2c-0.7,0.2-1.4,0.4-2.1,0.4c-3.6,0-6.5-2.9-6.5-6.5
							S14.3,25.1,17.9,25.1 M53.5,25.1c3.6,0,6.5,2.9,6.5,6.5c0,3.6-2.9,6.5-6.5,6.5c-0.3,0-0.6,0-0.9-0.1c-0.9-3.2-2.7-6.1-5.2-8.3
							C48.2,27.1,50.6,25.2,53.5,25.1 M36.2,35.9c3.6,0,6.5,2.9,6.5,6.5s-2.9,6.5-6.5,6.5s-6.5-2.9-6.5-6.5S32.6,35.9,36.2,35.9 M58,46.8
							c5.2,0,9.4,4.2,9.4,9.4v8.2c-2,0.7-4.3,1.3-6.8,1.6c-0.3-6.5-3.8-12.3-9.3-15.7c0.6-1.1,1-2.2,1.3-3.4H58 M19.9,46.9
							c0.3,1.1,0.7,2.2,1.3,3.3c-5.4,3.4-8.9,9.2-9.3,15.8c-2.6-0.3-5.2-0.9-7.8-1.6v-8.1c0-5.2,4.2-9.4,9.4-9.4H19.9 M40.7,57.6
							c2.5,0,4.9,1,6.6,2.8c1.7,1.8,2.7,4.1,2.7,6.5l0,0v0v8.2c-2.1,0.8-6.4,2-12.7,2c-4.9,0-9.9-0.7-14.9-2.1V67c0-5.2,4.2-9.4,9.4-9.4
							H40.7 M35.7,0c-6.3,0-11.4,5.1-11.4,11.4c0,6.2,5.1,11.3,11.4,11.3c6.3,0,11.4-5,11.4-11.3C47.1,5.1,42,0,35.7,0L35.7,0z M53.5,21.1
							c-5.7,0.1-10.3,4.7-10.4,10.4c3.3,2.1,5.6,5.6,6,9.6c1.3,0.6,2.8,1,4.4,1c5.8,0,10.5-4.7,10.5-10.5S59.3,21.1,53.5,21.1L53.5,21.1z
							 M17.9,21.1c-5.8,0-10.5,4.7-10.5,10.5c0,5.8,4.7,10.5,10.5,10.5c2,0,3.9-0.6,5.6-1.6c0.5-3.3,2.3-6.3,4.9-8.3c0-0.2,0-0.4,0-0.6
							C28.4,25.8,23.7,21.1,17.9,21.1L17.9,21.1z M40.5,23.5h-9.6c-1,0-2,0.1-3,0.3c1.5,2,2.3,4.5,2.3,7.2c1.8-1,3.8-1.5,6-1.5
							c0.2,0,0.4,0,0.5,0c1.6,0,3,0.4,4.5,1c0.1-2.5,1-4.8,2.3-6.7C42.5,23.6,41.5,23.5,40.5,23.5L40.5,23.5z M36.2,31.9
							c-5.8,0-10.5,4.7-10.5,10.5c0,5.8,4.7,10.5,10.5,10.5c5.8,0,10.5-4.7,10.5-10.5C46.7,36.6,42,31.9,36.2,31.9L36.2,31.9z M58,42.8
							h-8.8c-0.1,3.5-1.6,6.7-4,9c6.6,2,11.4,8,11.4,15.2v3.3c8.7-0.3,13.7-2.8,14-2.9l0.7-0.4h0.1V56.2C71.4,48.8,65.4,42.8,58,42.8
							L58,42.8z M23.3,42.9c-0.3,0-0.6,0-1,0h-8.9C6,42.9,0,48.9,0,56.3v10.9v0.2l0.7,0.2c5.7,1.8,10.7,2.6,15.1,2.8v-3.3
							c0.1-7.3,4.9-13.4,11.5-15.3C24.9,49.5,23.4,46.4,23.3,42.9L23.3,42.9z M40.7,53.6h-8.9c-7.4,0-13.4,6-13.4,13.4v10.9v0.2l0.7,0.2
							c7,2.2,13.2,2.9,18.2,2.9c9.8,0,15.5-2.8,15.9-3l0.7-0.4H54V67C54.1,59.6,48,53.6,40.7,53.6L40.7,53.6z"/>
						</svg>
					</div>
					<h1 style="font-size: 4em; margin: 5% 0;">Painéis</h1>
					<p style="font-size: 1.8em; line-height: 1.6em;">Debate de ideias e temas relevantes feito por profissionais com participação aberta ao público.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="container flex trilha">
			<div class="mobile-one-whole col-1-3 fl rl">
				<div class="trilha__item trilha__item--um">
					<h1 style="font-size: 4em; font-weight: 700;">02 dias de evento</h1>
					<h2 style="font-size: 2em; font-weight: 700;">21 e 22 de Setembro</h2>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/marca.png" alt="" style="position: absolute;top: -24px;right: -70px;" class="trilha__marca">
				</div>
				<div class="trilha__item trilha__item--dois">
					<h1 style="font-size: 4em; font-weight: 700;">Feira de Negócios<br>com 20 expositores</h1>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/marca.png" alt="" style="position: absolute;right: -80px;top: 0;" class="trilha__marca">
				</div>
			</div>
			<div class="mobile-one-whole col-1-3 fl linha">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/linha.png" alt="" style="width: 100%;height: 100%;" class="">
			</div>
			<div class="mobile-one-whole col-1-3 fl rl">
				<div class="trilha__item trilha__item--tres">
					<h1 style="font-size: 4em; font-weight: 700;">+ de 30<br>Palestras</h1>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/marca.png" alt="" style="position: absolute;top: -10px;left: -70px;" class="trilha__marca">
				</div>
				<div class="trilha__item trilha__item--quatro">
					<h1 style="font-size: 4em; font-weight: 700;">1.000<br>participantes</h1>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/marca.png" alt="" style="position: absolute;top: 20px;left: -70px;" class="trilha__marca">
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</section>
	<section id="palestrantes" class="palestrantes">
		<div class="container">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/capacete.png" alt="" style="width: 50%;position: absolute;left: -52%;top: 20%;z-index: 999;">
			<h1 style="margin: 4% 0 6%; margin-left: 10%;" class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-palestrantes.svg" alt="" style="width: 60px;float: left;margin-left: -70px;margin-top: -20px;"></div>Palestrantes confirmados</h1>
		<div class="col-1-1 flex">
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-caio.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/caiocmgo/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Caio Camargo</h1>
					<h2 class="palestrantes__sub-title">Fundador @ Falando de Varejo</h2>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-caio.jpg" alt="" class="mobile-one-whole col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/caiocmgo/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Caio Camargo</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Fundador @ Falando de Varejo</p>
							<p style="font-size: 1.6em; line-height: 1.4em;margin-bottom: 5%;">Caio Camargo é autor e editor do blog Falando de Varejo, no ar desde julho de 2008.</p>
							<p style="font-size: 1.6em; line-height: 1.4em;margin-bottom: 5%;">Considerado um dos maiores especialistas em varejo no Brasil, é formado em arquitetura pela Universidade São Marcos e MBA em Marketing pela FGV. É Diretor de Relações Institucionais da Virtual Gate, especializada em soluções de contagem de pessoas em pontos-de-venda, revolucionando o universo de métricas do varejo.</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">É palestrante, colunista oficial do portal Administradores e do Portal Novarejo. Articulista de diversos sites e revistas especializados.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-christian.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/christianmanduca/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Christian Manduca</h1>
					<p class="palestrantes__sub-title">Gerente comercial @ Verde Campo</p>
					<p class="palestrantes__sub-title">Fundador e Vice-Presidente @ Grupo Minas Trade Marketing</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-christian.jpg" alt="" class="mobile-one-whole col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/christianmanduca/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Christian Manduca</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Gerente comercial @ Verde Campo<br>Fundador e Vice-Presidente @ Grupo Minas Trade Marketing</p>
							<p style="font-size: 1.6em; line-height: 1.4em;margin-bottom: 5%;">Experiência de 18 anos na área Comercial, Marketing, Trade Markerting, Inteligência de Vendas, de Mercado e Desenvolvimento de Canais, em empresas como Laticínios Bom Gosto e Coca-Cola / Heineken. Economista formado pela PUCMinas, Especialista em Marketing pelo IBMEC-MG, Mestre em Administração (com linha de pesquisa em Trade Markerting) na Fundação Pedro Leopoldo. Fundador e Vice-Presidente do Minas Trade Markerting – Grupo Trade Markerting de Minas Gerais, Diretor de Trade e Shopper Marketing na Academia Mineira de Marketing – AMMA.</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Professor de Marketing, Trade Markerting, Shopper Marketing, Marketing de Varejos, Pricing, Gestão Estratégica de Canais, Omni-Channel, entre outras. Nas instituições: IBMEC, Fundação Dom Cabral e BI International, PUCMinas, FUMEC, UNI-BH, Newton Paiva. Coautor do Livro Trade Markerting – Pontos de Vista Expandidos.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-fatima.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/fatima-merlin-4303b3a/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Fátima Merlin</h1>
					<p class="palestrantes__sub-title">Especialista em Shopper e Varejo @ Connect Shopper</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-fatima.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/fatima-merlin-4303b3a/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Fátima Merlin</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Especialista em Shopper e Varejo @ Connect Shopper</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Tem mais de 20 anos de experiência, e desenvolveu projetos nacionais e internacionais para indústrias e varejos como Eudora, Pepsico, Kraft, Danone, Coop, GPA, Carrefour, WalMart.  com ênfase em inteligência de mercado, shopper insights, gerenciamento por categoria e shopper marketing. Palestrante em diversos eventos empresariais e instituições de renome como Apas, Abras, ASPB, AMAS, Sebrae, Abre, Anamaco, Insper, FGV, entre outros. Responsável pela criação e multiplicação dos cursos "Lidando com equipe vendedora foco no cliente" da Escola Nacional de Supermercados - Abras e "Gerenciamento por categoria na ótica do cliente" pela Escola Paulista de Supermercados - Apas. Além dos temas: Torne seus clientes fãs!, Atitude Vendedora!</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-olegario.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/oleg%C3%A1rio-ara%C3%BAjo-a450346/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Olegário Araújo</h1>
					<p class="palestrantes__sub-title">Diretor @ Inteligência de Varejo</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-olegario.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/oleg%C3%A1rio-ara%C3%BAjo-a450346/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Olegário Araújo</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Diretor @ Inteligência de Varejo</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Especialista em varejo, com atuação junto ao setor de FMCG, e diretor da agência de Inteligência de Varejo, cujo propósito é contribuir para que as organizações façam uso da informação de forma a otimizar seus resultados. É formado em administração de empresas pela PUC-SP, tem MBA em gestão de empresas pela FIA-USP, com extensão em varejo pela Youngstown State University, mestrando pela FGV e palestrante em eventos da Abras, Abad, Abrafarma, Acats, Agas, Amis, Apas, Apras, Beauty Fair, FGVcev, Pharmanager, Sindicom, entre outros.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-rodrigo.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/rodrigo-lamin-7424861b/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Rodrigo Lamin</h1>
					<p class="palestrantes__sub-title">CEO @ Involves</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-rodrigo.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/rodrigo-lamin-7424861b/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Rodrigo Lamin</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">CEO @ Involves</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Empreendedor e entusiasta de tecnologias voltadas ao ganho de performance em operações de trade marketing. Em 2008, nos últimos meses do curso de Sistemas de Informação na UFSC, fundou a Involves junto com outros 5 amigos. A startup de Florianópolis tornou-se uma das empresas que mais cresce no Brasil, fruto dos resultados com o Agile Promoter – plataforma em nuvem focada na gestão de operações de trade marketing e monitoramento de equipes de campo. Atualmente, o sistema já opera em mais de nove países. É também um dos idealizadores do Clube do Trade e do Agile Experience.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-rubens.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/rubens-sant%C2%B4anna-0b670429/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Rubens Sant’Anna</h1>
					<p class="palestrantes__sub-title">Sócio Diretor @ Grupo Sant´Anna Inamoto</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-rubens.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/rubens-sant%C2%B4anna-0b670429/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Rubens Sant’Anna</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Sócio Diretor @ Grupo Sant´Anna Inamoto</p>
							<p style="font-size: 1.6em; line-height: 1.4em;margin-bottom: 5%;">Sócio Diretor no Grupo Sant´Anna Inamoto. Consultor de Trade Marketing e Professor da ESPM, com MBA em Gestão Executiva e especializações em Branding e Psicologia do Consumidor.</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Atuação para empresas como Grendene, Johnson & Johnson, Souza Cruz, São Paulo Alpargatas, KraftFoods. Idealizador de diversos cursos de Trade Marketing. Co-autor do Livro “Trade Marketing – Pontos de Vista Expandidos” e autor do Livro “Trade Marketing – O domínio do canal de venda”. Palestrante nacional em eventos e conferências.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-simone.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/simoneterra/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Simone Terra</h1>
					<p class="palestrantes__sub-title">Professora  @ ESPM</p>
					<p class="palestrantes__sub-title">Fundadora @ STerra Soluções Estratégicas</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-simone.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/simoneterra/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Simone Terra</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Professora  @ ESPM<br>Fundadora @ STerra Soluções Estratégicas</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Possui mais de 20 anos de experiência nas áreas de trade marketing, gestão comercial, inteligência de varejo, merchandising, comportamento de compra, estudos de shopper e etnografia. Foi uma das precursoras em consultorias de Trade Marketing após ter introduzido os estudos de shopper no mercado brasileiro. Foi executiva do Instituto George Chetochine – França, onde fez sua especialização em Varejo e Comportamento de Compra. É formada em Ciências Sociais pela UFF, Pós-Graduada em Pesquisa de Mercado e Marketing Estratégico na França (ADETEM) e Mestranda em Economia Criativa (ESPM Rio), Professora da Pós-Graduação da ESPM/RJ, Sócia Diretora da Sterra Soluções de Mercado e Consultora da Dia Comunicação.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl rl palestrantes__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-tania.jpg" alt="" class="mobile-one-whole col-1-2 fl rl palestrantes__img--perfil">
				<div class="mobile-one-whole col-9-10 fr rl palestrantes__info--resumo">
					<a href="https://www.linkedin.com/in/taniazmine/" target="_blank" class="palestrantes__linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
					<h1 class="palestrantes__title">Tania Zahar Mine</h1>
					<p class="palestrantes__sub-title">Professora @ ESPM</p>
					<p class="palestrantes__sub-title">Fundadora @ Trad1e Design</p>
				</div>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/saiba-mais.jpg" alt="" class="fl btn--lightbox">
				<div class="lightbox" style="display: none;">
					<div class="mobile-one-whole tablet-one-half col-1-3 fl rl">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/palestrante-tania.jpg" alt="" class="col-1-2 fl rl" style="z-index: 99;position: absolute;left: -43%;top: -70px;">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="" style="z-index: 99;position: absolute; top: -70px; right: 0;" class="close">
						<div class="col-1-1 fr rl" style="background-color: #FFF; padding: 70px; box-sizing: border-box;">
							<a href="https://www.linkedin.com/in/taniazmine/" target="_blank" style="position: absolute;top: 5%;right: 2.5%;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-linkedin.jpg" alt=""></a>
							<h1 style="font-size: 2.2em; font-weight: 700; margin-bottom: 5%;">Tania Zahar Mine</h1>
							<p style="font-size: 1.6em; line-height: 1.4em; font-weight: 700;margin-bottom: 5%;">Professora @ ESPM<br>Fundadora @ Trade Design</p>
							<p style="font-size: 1.6em; line-height: 1.4em;">Formada em Publicidade e Propaganda pela Escola de Comunicações e Artes da Universidade de São Paulo, possui especialização em Administração com ênfase em Finanças pelo CEAG da FGV e MBA Executivo pela Fundação Dom Cabral. É mestre em Comunicação e Consumo do PPGCOM da ESPM e tem mais de 26 anos de experiência em grandes empresas como Unilever, Johnson & Johnson, Flora, Santher e Kimberly-Clark, tendo atuado em cargos executivos nas áreas de Marketing, Trade Marketing e Vendas. Atualmente, é diretora da Consultoria Trade Design e professora da pós-graduação da ESPM e da FIA.</p>
						</div>
					</div>
				</div>
			</article>
			<article class="mobile-one-whole tablet-one-half col-1-3 fl" style="margin-bottom: 5%"></article>
		</div>
		</div>
	</section>
	<section id="o-local" class="meio page page--dois">
		<div class="container">
			<div class="cont">
				<div class="local mobile-one-whole center">
					<h1 class="title" style="margin: 7% 0 4%;float: left;width: 100%;color: #6C0F0E;" class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-local.svg" alt="" style="width: 50px;float: left;margin-left: -60px;margin-top: -40px;"></div>O Local</h1>
					<div class="col-1-1 loc-cont">
						<div class="col-3-5 mobile-one-whole fl loc-cont--inner">
							<div class="fl"><p><strong>Costão do</strong></p>
							<p class="bottom"><strong>Santinho</strong></p></div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao-resort-golf-spa-05.png" alt="" class="col-3-10 mobile-three-fifths fl loc-logo">
							<div class="col-1-1 mobile-one-whole fl local__content">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/costao0.jpg" alt="" class="col-1-1" style="margin-top: 4%; margin-bottom: 4%;">
								<p>Com um dos maiores centros de convenções à beira-mar do país, o Costão do Santinho comporta eventos para até 4.000 pessoas, e servirá de palco para o AEx 2017.</p>
								<p>Uma estrutura com padrão internacional junto a comodidade do melhor resort de praia do Brasil. O Costão do Santinho recebe anualmente eventos premium dos mais variados segmentos.</p>
								<p>O AEx não poderia ter casa melhor do que essa.</p>
							</div>
							<a href="http://www.costao.com.br/" target="_blank" class="btn btn--local mobile-nine-tenths">Saiba mais</a>
						</div>
						<div class="col-2-5 mobile-one-whole fl loc-img" data-aos="fade-up" data-aos-duration="600">
							<img src="http://www.costao.com.br/images/espacos/espaco-tugua/5.jpg" alt="" class="col-1-1" style="margin-bottom: 5%;">
							<img src="http://www.costao.com.br/images/espacos/espaco-tugua/1.jpg" alt="" class="col-1-1" style="margin-bottom: 5%;">
							<img src="http://www.costao.com.br/images/espacos/espaco-tugua/7.jpg" alt="" class="col-1-1">
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</section>
	<section id="hospedagem" class="hospedagem meio page">
		<div class="container">
			<div class="col-1-1 hospedagem__cont">
				<h1 class="title" style="margin: 7% 0 4%;float: left;width: 100%;color: #6C0F0E;" class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hospedagem.svg" alt="" style="width: 50px;float: left;margin-left: -60px;margin-top: -15px;"></div>Hospedagem</h1>
				<div class="col-1-1 hosp-cont">
					<div class="mobile-one-whole col-3-5 fl hosp-cont--inner">
						<p><strong>Costão do Santinho</strong></p>
						<p class="bottom"><strong>Hotel oficial<br>AEx 2017</strong></p>
						<div class="col-1-1 mobile-one-whole fl hospedagem__content">
							<p>O melhor resort de praia do Brasil, para a melhor e maior experiência em trade marketing.</p>
							<p>Estrutura completa para um grande evento com hospedagem em um hotel referência no Brasil.</p>
							<p>Sua participação no evento ficará muito mais cômoda e agradável ao desfrutar de tudo o que o Costão do Santinho oferece.</p>
							<ul class="cards__lista-plus">
								<li class="cards__lis-item-plus">Diárias a partir de R$ 407,50* a partir de apartamento duplo</li>
								<li class="cards__lis-item-plus">Pensão completa de alimentação com bebidas não alcoólicas:
									<ul style="text-align: left;padding: 2.5% 0 0 18px;list-style: initial;font-weight: 400;">
										<li>Café da manhã</li>
										<li>Almoço gourmet livre</li>
										<li>Jantar (gastronomia italiana, japonesa e contemporânea)</li>
									</ul>
								</li>
								<li class="cards__lis-item-plus">Trilhas arqueológicas guiadas</li>
								<li class="cards__lis-item-plus">Complexo de piscinas, sendo uma aquecida.</li>
								<li class="cards__lis-item-plus" style="border: 0;">Acesso direto à praia</li>
							</ul>
							<p style="padding: 3% 5% 0;font-size: 1.6em;">* acréscimo de 6.15% refente à taxas</p>
						</div>
						<div class="col-1-2 fl" style="font-weight: 700; font-size: 1.6em; width: 90%;">
							<p>Preencha os campos abaixo para receber mais informações sobre hospedagem.</p>
							<p>Nossa equipe entrará em contato!</p>
						</div>
						<form class="col-1-1 mobile-one-whole fl" id="form-hosp">
							<input type="text" placeholder="Nome" id="nome" name="nome" class="form__field" style="margin-left: 0;">
							<input type="text" placeholder="E-mail" id="email" name="email" class="form__field" style="margin-left: 0;">
							<input type="text" placeholder="Telefone" id="telefone" name="telefone" class="form__field telefone" style="margin-left: 0;">
							<div class="clear"></div>
							<input type="submit" class="btn btn--submit" value="Enviar" style="margin-left: 0;">
						</form>
					</div>
					<div class="col-2-5 mobile-one-whole fl slick hosp-slide">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hosp-img1.jpg" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hosp-img2.jpg" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hosp-img3.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</section>
	<section id="aliste-se" style="background-color: #000;">
		<div class="container">
			<h1 class="title" style="margin: 7% 0 4%;padding-left: 10%;float: left;width: 100%;color: #FFF;"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-inscricoes.svg" alt="" style="width: 50px;float: left;margin-left: -60px;margin-top: -45px;"></div>Aliste-se</h1>
			<div class="list col-1-2 mobile-one-whole fl">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/lista.png" alt="" style="margin-left: 6%;">
			</div>
			<div class="col-1-2 mobile-one-whole fl pre">
				<p class="paragraph paragraph--pre-venda">
					<span style="background-color: #fbbf42;color: #000;padding: 6px;border-radius: 4px;font-weight: 700;display: block;width: 100px;font-size: 1.6em;text-align: center;"><span style="transform: rotate(-6deg); display: inline-block;">Sold Out</span></span>
					<span style="text-decoration: line-through;">PRÉ-VENDA R$ 350,00</span>
				</p>
				<p class="paragraph paragraph--pre-venda mBottom"><small style="font-style: italic;">À partir de</small><i class="italic"> R$ 540,00</i><br><small>em até 10 vezes.</small><br><small>1º lote até 28/04/17</small></p>
				<a href="https://www.sympla.com.br/agile-experience17__117773" class="btn btn--action" target="_blank">Compre agora</a>
			</div>
			<div class="clear"></div>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bandeira.png" alt="" style="width: 30%;position: absolute;right: -80px;bottom: 0%;z-index: 999;">
		</div>
	</section>
	<section id="faq" class="no-overflow">
		<div class="faq container mobile-one-whole center">
			<h1 class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-faq.svg" alt="" style="width: 50px;float: left;margin-left: -60px;margin-top: -15px;"></div>FAQ</h1>
			<div class="fl col-1-2 mobile-one-whole">
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Vocês emitem certificado online?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		              <div class="tab__paragraph">Sim. Os certificados serão enviados via e-mail pela equipe organizadora em até uma semana após o evento.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Como pagar a minha inscrição?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		              <div class="tab__paragraph">Todas as inscrições podem ser compradas por boleto bancário, débito online e cartão de crédito (10x).</div>
		              <div class="tab__paragraph">Os pagamentos realizados do exterior somente podem ser processados em uma parcela e em moeda local.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Vocês emitem nota fiscal?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		              <div class="tab__paragraph">Sim. Uma vez confirmado o pagamento, sua nota fiscal seguirá em até duas semanas subsequente à data de compra para o e-mail informado no momento do cadastro. Mais informações <a href="mailto:financeiro@involves.com.br" style="color: #a9120f;">financeiro@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Incide algum imposto na minha compra? Qual?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">“Ressaltamos que esta nota fiscal contempla o serviço 12.08 de feiras, exposições, eventos e congressos. Portanto, o tomador do serviço não deve realizar retenções Federais e/ou Municipais. Legislação: IRRF RIR/1999, PIS/COFINS/CSLL Lei Nº 10.833/2003 e ISS Lei Complementar 116/2003”</div><div class="tab__paragraph">Uma vez confirmado o pagamento, sua nota fiscal seguirá em até duas semanas após a compra para o e-mail informado no momento do cadastro.</div>
						<div class="tab__paragraph">SERÁ EMITIDA SOMENTE UMA NOTA FISCAL POR PEDIDO.</div>
						<div class="tab__paragraph">Mais informações <a href="mailto:financeiro@involves.com.br" style="color: #a9120f;">financeiro@involves.com.br</a>.</div>
					</div>
				</div>
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Como funcionará a inscrição no evento?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		              	<div class="tab__paragraph">Nosso credenciamento estará funcionando meia hora antes da primeira palestra, no local do evento. Chegue cedo e evite filas. Não esqueça de trazer a impressão do ingresso recebido por e-mail. É indispensável a apresentação de documento de identidade com foto.</div>
		            	<div class="tab__paragraph">Os participantes do Agile Experience estão cientes e concordam que fotografias e filmagens feitas durante o evento poderão ser utilizadas pelos organizadores para promoção e divulgação do mesmo.</div>
		            	<div class="tab__paragraph">Mais informações: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a></div>
		            </div>
				</div>
				<div class="tab__all tab__all--right">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>A hospedagem no Costão do Santinho está inclusa no ingresso?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Não, mas são oferecidos descontos para participantes do evento.</div>
		            </div>
				</div>
			</div>
			<div class="fl col-1-2 mobile-one-whole">
				<div class="tab__all tab__all--left">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Existe algum desconto para a compra em grupo?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Sim! Se você ou sua empresa realizarem a compra de duas ou mais inscrições está garantindo um desconto de 10%. Exceto para os ingressos de pré-vendas. </div>
		            </div>
				</div>
				<div class="tab__all tab__all--left">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Não recebeu o seu e-mail de confirmação?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Depois de fazer login em sua conta na <a href="https://www.sympla.com.br/agile-experience17__117773" target="_blank" style="color: #a9120f;">Sympla</a>, acesse o menu da conta e selecione Ingressos. Caso não tenha recebido a confirmação da sua compra em até 48h, entre em contato pelo e-mail: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--left">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Posso cancelar a minha inscrição?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Sim. Cancelamentos serão aceitos até 07 (sete) dias corridos após a compra do ingresso, desde que 48h antes do evento, sem exceção. Caso o ingresso tenha sido comprado pela opção da compra em grupo, o cancelamento de uma inscrição cancela todo o pacote do grupo, tendo que ser refeito o processo de compra.</div>
		            	<div class="tab__paragraph">Dúvidas? Entre em contato através: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--left">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Posso transferir a minha inscrição?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Transferência de titularidade será aceita desde que 48h antes da data do evento. Entre em contato através do e-mail: <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="tab__all tab__all--left">
					<h1 class="tab__header col-1-1 mobile-one-whole fl rl">
						<div>Como faço para patrocinar o AEx2017?</div>
						<span class="tab__btn tab__btn--open">+</span>
						<span class="tab__btn tab__btn--close">-</span>
					</h1>
		            <div class="tab__item col-1-1">
		            	<div class="tab__paragraph">Para mais informações sobre as cotas de patrocínio envie um e-mail para <a href="mailto:marketing@involves.com.br" style="color: #a9120f;">marketing@involves.com.br</a>.</div>
		            </div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/barreira.png" alt="" style="width: 55%;position: absolute;bottom: -15%;z-index: 999;">
	</section>

	<section id="contato" class="contato">
		<div class="container">
			<span class="alert alert--error">Por favor preencha todos os campos!</span>
    		<span class="alert alert--success">Seus dados foram enviados.</span>
			<div class="col-1-1 mobile-nine-tenths center contato__container">
				<div class="col-1-2 mobile-one-whole fl">
					<h1 class="title"><div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-contato.svg" alt="" style="width: 35px;float: left;margin-left: -60px;margin-top: -40px;"></div>Contato</h1>
					<div class="clear"></div>
					<h2 class="contato__title" style="width: 80%;">Fale com a gente para saber mais sobre o evento.</h2>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/jipe.png" alt="" style="width: 100%;">
				</div>
				<div class="col-1-2 mobile-one-whole rl fl">
					<form class="form" id="formulario">
						<input type="text" placeholder="Nome" id="nome" name="nome" class="form__field">
						<input type="text" placeholder="Empresa" id="empresa" name="empresa" class="form__field">
						<input type="text" placeholder="E-mail" id="email" name="email" class="form__field">
						<input type="text" placeholder="Telefone" id="telefone" name="telefone" class="form__field telefone">
						<textarea name="mensagem" id="mensagem" placeholder="Mensagem" class="form__field form__field--message"></textarea>
						<input type="submit" class="btn btn--submit" value="Enviar">
					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="patrocinar">
		<a href="http://agilexperience.com.br/patrocinio/" class="center">Quero patrocinar</a>
	</section>

	<footer class="footer">
		<div class="container">
			<div class="col-4-5 flex center">
				<div class="footer__info col-1-2 mobile-one-whole flex">
					<p>Uma iniciativa</p>
					<a href="http://www.involves.com.br/" target="_blank" class="footer__logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-involves.svg" alt="" style="height: 34px;"></a>
				</div>

				<div class="footer__cont col-1-2 mobile-one-whole">
	    			<p class="fr">+55 11 4063.6633  -  <a href="mailto:marketing@involves.com.br">marketing@involves.com.br</a></p>
	    		</div>
	    		<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>