$(document).mousemove(function(e) {
    if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        $('.background').parallax(40, e);
        $('.txt1').parallax(35, e);
        $('.cortada').parallax(25, e);
        $('.txt2').parallax(25, e);
        $('.isall').parallax(15, e);
    }
});
jQuery(function($){
   $("#telefone").mask("(99) 9999-9999?9");
});

$(document).ready(function() {
  /**
   * Video element
   * @type {HTMLElement}
   */
  var video = document.getElementById("my-video");

  /**
   * Check if video can play, and play it
   */
  video.addEventListener( "canplay", function() {
    video.play();
  });
});

$(document).ready(function() {
    AOS.init({
        easing: 'ease-in-out-sine'
    });
    $('.slick--parallax').slick({
        dots: false,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        speed: 1200,
        pauseOnHover: false
    });
     $('.slick').slick({
        dots: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        speed: 1200,
        pauseOnHover: false
    });
});
$('.scroll').click(function(event) {
    event.preventDefault();
    var full_url = this.href;
    var parts = full_url.split('#');
    var trgt = parts[1];
    var target_offset = $('#' + trgt).offset();
    var target_top = target_offset.top;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('html, body').animate({
            scrollTop: target_top - 62
        }, 500);
    } else {
        $('html, body').animate({
            scrollTop: target_top - 65
        }, 500);
    }
});

$("#hamburger").click(function(s) {
    $(".header").toggleClass("is-expanded");
});
$(".nav__link").click(function(s) {
    $(".header").toggleClass("is-expanded");
});
$(".tab__header").click(function(s) {
    $(this).parent().toggleClass("is-open");
});
$(window).scroll(function() {
    var height = $('.header').height();
    var nav = $('.header');
    if ($(this).scrollTop() > height) {
        nav.addClass("is-scroll");
    } else {
        nav.removeClass("is-scroll");
    }
});
$('#formulario').on('submit', function(e){
    var dados = $(this).serialize();
    var nome        = $("#nome").val();
    var empresa     = $("#empresa").val();
    var email       = $("#email").val();
    var telefone    = $("#telefone").val();
    var mensagem    = $("#mensagem").val();

    if(nome=='' || mensagem==''|| email==''){
        $('.alert--success').fadeOut(800).hide();
        $('.alert--error').fadeIn(800, function() {
            $('.alert--error').fadeOut(800);
        }).show().delay(1500);
    }else{
        $.ajax({
            type: "POST",
            url: "https://agilexperience.com.br/send-form",
            data: dados,
            success: function( data ){
                $('.alert--success').fadeIn(800, function() {
                    $('.alert--success').fadeOut(800);
                }).show().delay(1500);
                $('.alert--error').fadeOut(800).hide();
                $('#formulario').each (function(){
                    this.reset();
                });
            }
        });
    }
    return false;
});
$(".btn--lightbox").click(function() {
    $(this).next().fadeIn();
});
$(".lightbox .close").click(function() {
    $(".lightbox").fadeOut();
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
         $(".lightbox").fadeOut();
    }
});


function perspectiveHover(el, parent, intensity) {

    var self         = this,
        elClass      = el.replace(/\./g,''),
        parentClass  = parent.replace(/\./g,''),
        parent       = document.getElementsByClassName(parentClass),
        $parent      = $(parent),
        els          = document.getElementsByClassName(elClass);

    this.perspective = function(e, el) {

        var elX      = el.getBoundingClientRect().left,
            elY      = el.getBoundingClientRect().top,
            elWidth  = el.offsetWidth,
            elHeight = el.offsetHeight;

        var mouseX = e.clientX,
            mouseY = e.clientY;

        var valY   = -( ( elWidth/2 - (mouseX - elX) ) / (elWidth/2) * intensity ),
            valX   = ( elHeight/2 - (mouseY - elY) ) / (elHeight/2) * intensity;

        el.style.webkitTransform = 'rotateX('+ valX.toFixed(1) + 'deg) rotateY(' + valY.toFixed(1) + 'deg)'; // need webkit transform for this to work in safari 8
        el.style.transform       = 'rotateX('+ valX.toFixed(1) + 'deg) rotateY(' + valY.toFixed(1) + 'deg)'; // toFixed to round decimal values

    }

    this.anim = function(el, valX, valY, time) {

        animate({
            time: time,  // time in seconds
            run: function(rate) {
                el.style.webkitTransform = 'rotateX('+ rate*valX +'deg) rotateY('+ rate*valY +'deg)';
                el.style.transform = 'rotateX('+ rate*valX +'deg) rotateY('+ rate*valY +'deg)';
            }
        });

        function animate(item) {

            var duration = 1000*item.time,
                end = +new Date() + duration;

            var step = function() {

                var current = +new Date(),
                    remaining = end - current;

                if( remaining < 60 ) {
                    item.run(0);  // 1 = progress is at 100%
                    return;

                } else {
                    var rate = remaining/duration;
                    item.run(rate);
                }
                window.requestAnimationFrame(step);
            }
            step();
        }
    }

    this.getTransforms = function(el) {

        var st           = window.getComputedStyle(el, null),
            // tr           = st.getPropertyValue("transform"),
            matrix       = st.getPropertyValue("transform"),
            webkitMatrix = st.getPropertyValue("-webkit-transform"),
            rotateX = 0,
            rotateY = 0,
            rotateZ = 0;

        if (matrix !== 'none') {       

            // for safari
            if (!webkitMatrix == '') {
                matrix = webkitMatrix;
            }

            // calculate the values of the rotation
            var values      = matrix.split('(')[1].split(')')[0].split(','),
                pi          = Math.PI,
                sinB        = parseFloat(values[8]),
                b           = Math.asin(sinB) * 180 / pi,
                cosB        = Math.cos(b * pi / 180),
                matrixVal10 = parseFloat(values[9]),
                a           = Math.asin(-matrixVal10 / cosB) * 180 / pi,
                rotateX = a;
                rotateY = b;

            return [rotateX, rotateY];
            
        }

    }

    $parent.on('mousemove', el, function(e){
        self.perspective(e, this);
    });

    $parent.on('mouseleave', el, function(e){
        self.anim(this, self.getTransforms(this)[0], self.getTransforms(this)[1], 0.3);
    });    
}

perspectiveHover('.js-perspective-card', '.js-perspective', 8);