module.exports = function(grunt) {
  grunt.initConfig({
    uglify: {
      'assets/js/main.js' : ['assets/_js/*.js']
    }, //uglify

    less: {
      compress: {
        options: {
          paths: ['test/fixtures/include'],
          compress: true
        },
        files: {
          'assets/css/style.css' : ['assets/_less/style.less']
        }
      },
    }, //less

    imagemin: {                          // Task
      dynamic: {                         // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          optimizationLevel: 5,
          progressive: true,
          cwd: 'assets/_images/',        // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
          dest: 'assets/images2/'         // Destination path prefix
        }]
      }
    }, //imagemin

    htmlmin: {                                     // Task
      dist: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {                                   // Dictionary of files
          'index.html': 'assets/index.html',     // 'destination': 'source'
        }
      }
    }, // html

    watch : {
      dist : {
        files : [
          'assets/_js/**/*',
          'assets/_less/**/*'
        ],

        tasks : [ 'uglify', 'less' ]
      }
    } // watch
  });

  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Tarefas que serão executadas
  grunt.registerTask('default', ['uglify', 'less', 'imagemin']);

  // Tarefa para Watch
  grunt.registerTask( 'w', [ 'watch' ] );

  grunt.registerTask( 'img', [ 'imagemin' ] );

  grunt.registerTask( 'h', ['htmlmin'] );
};
