<?php
function load_fonts() {
  wp_register_style('Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,700');
  wp_enqueue_style( 'Montserrat');
}
add_action('wp_print_styles', 'load_fonts');

function add_theme_scripts() {
    if (is_home()){
        wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.css',false,'1.1','all');
        wp_enqueue_style( 'slickcss', get_template_directory_uri() . '/assets/slick/slick.min.css',false,'1.1','all');
        wp_enqueue_style( 'aoscss', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css',false,'1.1','all');
        wp_enqueue_script( 'all', 'https://code.jquery.com/jquery-2.1.4.min.js', array(), '2.1.4' , true );
        wp_enqueue_script( 'TweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js', array(), '1.17' , true );
        wp_enqueue_script( 'aos', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', array(), '2.1.1' , true );
        wp_enqueue_script( 'slickjs', get_template_directory_uri() . '/assets/slick/slick.min.js', array('all'), '1.1' , true );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array('all'), '1.1' , true );
    }
    if (is_page('virada-de-lote') ){
        wp_enqueue_style( 'style', get_template_directory_uri() . '/assets2/css/style.css',false,'1.1','all');
        wp_enqueue_style( 'slickcss', get_template_directory_uri() . '/assets2/slick/slick.min.css',false,'1.1','all');
        wp_enqueue_style( 'aoscss', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css',false,'1.1','all');
        wp_enqueue_script( 'all', 'https://code.jquery.com/jquery-2.1.4.min.js', array(), '2.1.4' , true );
        wp_enqueue_script( 'TweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js', array(), '1.17' , true );
        wp_enqueue_script( 'aos', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', array(), '2.1.1' , true );
        wp_enqueue_script( 'slickjs', get_template_directory_uri() . '/assets2/slick/slick.min.js', array('all'), '1.1' , true );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/assets2/js/main.js', array('all'), '1.1' , true );
    }
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

show_admin_bar(false);
?>
