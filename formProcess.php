<?php
	$errors = array();        // array to hold validation errors
	$data   = array();        // array to pass back data
   if(!empty($_REQUEST['action']) && $_REQUEST['action'] == "postFormData")	
    {	  
		  // validate the variables ========
		  if (empty($_POST['name']))
			$errors['name'] = 'Nome obrigatório.';
			
			if (empty($_POST['email']))
			$errors['email'] = 'E-mail obrigatório.';
		
		  if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false && $_POST['email']!="" ) {
     		$errors['email'] = 'E-mail inválido.';
           }	

           if (empty($_POST['phone']))
			$errors['phone'] = 'Telefone obrigatório.';
		  
		  // return a response ==============
		  
		  // response if there are errors
		  if ( ! empty($errors)) {
		  
			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		  } else {
		  
			// if there are no errors, return a message , process your form data as you want here
		 
		  

		  	$name     = $_POST['name'];
	    	$email    = $_POST['email'];
	    	$phone    = $_POST['phone'];
	    	$message  = $_POST['message'];
		  	$email_remetente = "globaltravelcorporate@globaltravelcorporate.com.br";

		  	$email_destinatario = "info@viajeglobal.com.br"; // qualquer email pode receber os dados
		  	$email_destinatario2 = "web@ezcuze.com.br"; // qualquer email pode receber os dados
			$email_reply = "$email";
			$email_assunto = "Contato - Pagina agência";

		  	$email_conteudo = '<P>Nome: '.$name.'</P><P>Email: '.$email.'</P><P>Telefone: '.$phone.'</P><P>Mensagem: '.$message.'</P>';
			
			$email_headers = implode ( "\n",array ( "From: $email_remetente", "Reply-To: $email_reply", "Return-Path:  $email_remetente","MIME-Version: 1.0","X-Priority: 3","Content-Type: text/html; charset=UTF-8" ) );

			$success = mail ($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers);
			mail ($email_destinatario2, $email_assunto, nl2br($email_conteudo), $email_headers);

			if($success){
				$data['message'] = "Mensagem enviada com sucesso. Em breve entraremos em contato.";
				$data['success'] = true;
			}
		  }
		  echo json_encode($data);
     }
?>