$(document).mousemove(function(e) {
    if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        $('.background').parallax(40, e);
        $('.txt1').parallax(35, e);
        $('.cortada').parallax(25, e);
        $('.txt2').parallax(25, e);
        $('.isall').parallax(15, e);
    }
});
jQuery(function($){
   $("#telefone").mask("(99) 9999-9999?9");
});
$(document).ready(function() {
    AOS.init({
        easing: 'ease-in-out-sine'
    });
    $('.slick--parallax').slick({
        dots: false,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        speed: 1200,
        pauseOnHover: false
    });
     $('.slick').slick({
        dots: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        speed: 1200,
        pauseOnHover: false
    });
});
$('.scroll').click(function(event) {
    event.preventDefault();
    var full_url = this.href;
    var parts = full_url.split('#');
    var trgt = parts[1];
    var target_offset = $('#' + trgt).offset();
    var target_top = target_offset.top;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('html, body').animate({
            scrollTop: target_top - 62
        }, 500);
    } else {
        $('html, body').animate({
            scrollTop: target_top - 65
        }, 500);
    }
});

$("#hamburger").click(function(s) {
    $(".header").toggleClass("is-expanded");
});
$(".nav__link").click(function(s) {
    $(".header").toggleClass("is-expanded");
});
$(".tab__header").click(function(s) {
    $(this).parent().toggleClass("is-open");
});
$(window).scroll(function() {
    var height = $('.header').height();
    var nav = $('.header');
    if ($(this).scrollTop() > height) {
        nav.addClass("is-scroll");
    } else {
        nav.removeClass("is-scroll");
    }
});
$('#formulario').on('submit', function(e){
    var dados = $(this).serialize();
    var nome        = $("#nome").val();
    var empresa     = $("#empresa").val();
    var email       = $("#email").val();
    var telefone    = $("#telefone").val();
    var mensagem    = $("#mensagem").val();

    if(nome=='' || mensagem==''|| email==''){
        $('.alert--success').fadeOut(800).hide();
        $('.alert--error').fadeIn(800, function() {
            $('.alert--error').fadeOut(800);
        }).show().delay(1500);
    }else{
        $.ajax({
            type: "POST",
            url: "https://agilexperience.com.br/send-form",
            data: dados,
            success: function( data ){
                $('.alert--success').fadeIn(800, function() {
                    $('.alert--success').fadeOut(800);
                }).show().delay(1500);
                $('.alert--error').fadeOut(800).hide();
                $('#formulario').each (function(){
                    this.reset();
                });
            }
        });
    }
    return false;
});
